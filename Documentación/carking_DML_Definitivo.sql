-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-06-2021 a las 21:45:42
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carking`
--
CREATE DATABASE IF NOT EXISTS `carking` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `carking`;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idarticulo`, `idautor`, `titulo`, `contenido`, `imgsrc`, `fechapub`) VALUES
(1, 1, 'Dónde cargar un coche eléctrico: opciones y mapas de puntos disponibles', '<p>Poco a poco, el vehículo eléctrico comienza a introducirse en la rutina diaria de las ciudades, así como de muchas personas que lo eligen como medio de transporte. Pero, ¿dónde puede cargarse y cómo saber los puntos de recarga a los que podemos acudir?</p>\r\n\r\n<p>Aunque la tecnología eléctrica en el sector de la automoción avanza a buen ritmo, lo cierto es que aún existen retos para aquellos que opten por un coche eléctrico como medio de transporte, especialmente si necesitan moverse por las carreteras y autovías.</p>', 'http://localhost/carking/carking-API/assets/upload/60b66eaa1bfee.png', '2021-05-16 09:49:04'),
(18, 1, ' ¿Qué son los aparcamientos públicos y cuáles son sus tipos? ', '<p>La Ley 40/2002, reguladora del contrato de aparcamiento, regula el régimen aplicable a los aparcamientos en los que\n  una\n  persona titular cede, como actividad mercantil, un espacio en un local o recinto para el estacionamiento de vehículos\n  a\n  motor. Esto supone el cumplimiento de una serie de deberes de vigilancia y custodia durante el tiempo de ocupación, a\n  cambio de un precio determinado en función del tiempo real de prestación del servicio.</p>\n\n<p>Se consideran dos modalidades de prestación de este tipo de servicio:<br><br><br>\n\n  Estacionamiento o aparcamiento con reserva de plaza: la titularidad del aparcamiento queda obligada a mantener durante\n  todo el periodo de tiempo pactado una plaza de aparcamiento a disposición plena de la persona usuaria.<br><br>\n\n  Estacionamiento o aparcamiento rotatorio: la titularidad del aparcamiento se obliga a facilitar una plaza de\n  aparcamiento por un periodo de tiempo variable, no prefijado. En esta modalidad, se pactará el precio por minuto de\n  estacionamiento, sin posibilidad de redondeos a unidades de tiempo no efectivamente consumidas o utilizadas.<br><br>\n</p>\n\n<p>excluyen del concepto de aparcamiento establecido en la Ley 40/2002:<br><br><br>\n\n  Estacionamientos en las denominadas zonas de estacionamiento regulado o en la vía pública (tanto si exigen el pago de\n  tasas, como si no).<br><br>\n\n  Estacionamientos no retribuidos directa o indirectamente.\n</p>\n\n\n<p>Si necesita información en materia de consumo no dude en contactar con nosotros. Le recordamos que estamos a su\n  disposición de forma gratuita y continuada a través del número de teléfono 900 21 50 80, del correo electrónico\n  consumoresponde@juntadeandalucia.es, así como en nuestros perfiles de redes sociales o a través de esta misma página\n  Web. Y si prefiere un servicio de atención presencial, puede acercarse a alguno de los Servicios Provinciales de\n  Consumo, presentes en todas las capitales de provincia andaluzas.</p>', 'http://localhost/carking/carking-API/assets/upload/60b67fc2b407c.png', '2021-06-01 18:43:14'),
(20, 1, 'Alarmas, sensores de aparcamiento y otros artículos para equipar tu coche', '<p>Si necesita información en materia de consumo no dude en contactar con nosotros. Le recordamos que estamos a su\n    disposición de forma gratuita y continuada a través del número de teléfono 900 21 50 80, del correo electrónico\n    consumoresponde@juntadeandalucia.es, así como en nuestros perfiles de redes sociales o a través de esta misma página\n    Web. Y si prefiere un servicio de atención presencial, puede acercarse a alguno de los Servicios Provinciales de\n    Consumo, presentes en todas las capitales de provincia andaluzas.</p>\n\n<p>\n    Es evidente que la función principal del coche es movernos con facilidad y rapidez en trayectos cortos o largos.\n    Pero si hay un sector que aprovecha cada uno de los avances tecnológicos es el de la automoción, ya que los nuevos\n    modelos que salen al mercado cada año incorporan novedosos sistemas y dispositivos que facilitan la conducción y\n    proveen entretenimiento y seguridad en cada trayecto.</p>\n\n<p>\n    Sin embargo, mientras la tecnología avanza, los modelos de coches de años anteriores se van quedando atrás en estas\n    novedades; algo que también ocurre con las gamas más austeras de vehículos. Es por eso que en EL PAÍS Escaparate\n    hemos elaborado una selección de gadgets para equipar el coche con lo último en tecnología. Con estos dispositivos,\n    cualquier automóvil puede actualizarse con equipo de entretenimiento, seguridad, navegación, aparcamiento y\n    complementos para usar el móvil o la tablet a bordo.\n</p>\n<br><br>\n<h4>Entretenimiento</h4>\n<h5>Autoradio bluetooth, reproductor MP3 y manos libres</h5>\n\n<p>Se trata de un artículo muy completo, que no puede faltar en los vehículos para amenizar las jornadas de conducción,\n    y que permite realizar y recibir llamadas, escuchar la radio, utilizar sistemas bluetooth (lleva micrófono\n    incorporado) o reproducir música desde un USB. Soporta formatos MP3 y WMA. El kit incluye también un conversor ISO\n    para la conexión de los cables junto con la radio y un mando a distancia. Las dimensiones del panel de radio son 188\n    milímetros (mm) x 58 mm x 76 mm.</p>\n<br>\n<h5>Transmisor FM y MP3, bluetooth y cargador USB</h5>\n<p>\n    Otro aparato muy práctico para el coche es este que se coloca en la zona del mechero y tiene múltiples funciones. A\n    través de un bluetooth 4.1 puede reproducir música desde dispositivos habilitados para ello. Además, lleva\n    incorporado un micrófono para responder o hacer llamadas desde el móvil de manera sencilla, cuenta con dos puertos\n    USB y 4 modos de reproducción de música: mediante señal FM, de una la tarjeta TF (hasta 32 GB), archivos como MP3 o\n    desde un disco flash USB (hasta 32 GB). En Amazon está disponible, por el mismo precio, en dos colores: negro y\n    gris.\n</p>', 'http://localhost/carking/carking-API/assets/upload/60b68285defad.png', '2021-06-01 18:55:01');

--
-- Volcado de datos para la tabla `faq`
--

INSERT INTO `faq` (`idfaq`, `idautor`, `pregunta`, `respuesta`) VALUES
(1, 1, '¿Cuánto cuesta utilizar Carking?', 'Como usuario puedes disfrutar de esta aplicación de forma totalmente gratuita'),
(2, 1, '¿Debo registrarme?', 'Para poder ofrecerte un servicio personalizado hemos estimado que debes registrarte.'),
(3, 1, '¿La aplicación funciona sin internet?', 'Es algo que es imprescindible. Además estamos trabajando para poder ofrecer el servicio a través de una aplicación móvil.'),
(4, 1, '¿De que forma puedo reservar una plaza?', 'Para poder reservar una plaza debes registrar un vehículo en la plataforma y acompañando a esto, debes aceptar que la aplicación tenga acceso a tu geolocalización, tras esto, en el mapa de la aplicación verás las plazas disponibles en cada momento.');

--
-- Volcado de datos para la tabla `parking`
--

INSERT INTO `parking` (`idparking`, `nombre`, `direccion`, `numero`, `poblacion`, `provincia`, `codigopostal`, `tel`, `web`, `correo`, `preciomin`, `preciomedh`, `precioh`, `preciodia`, `preciomaxd`, `apertura`, `cierre`, `estadocierre`) VALUES
(2, 'Zona azul', 'C/ Roldán', '9', 'benidorm', 'alicante', '03500', '966806134', 'https://www.oragruabenidorm.es/ora/zona-azul/', 'info@oragruabenidorm.es', '0.04', '0.30', '0.60', '10.00', '12.00', '06:00:00', '21:00:00', 'Cerrado'),
(3, 'Parking público ayto. Benidorm', 'Plaza de SS. MM. Los Reyes de España', '1', 'Benidorm', 'alicante', '03501', '966815400', 'https:/benidorm.org/', 'incidencias@benidorm.org', '0.00', '0.00', '0.00', '0.00', '0.00', '00:00:00', '00:00:00', 'Público');

--
-- Volcado de datos para la tabla `plaza`
--

INSERT INTO `plaza` (`idplaza`, `idparking`, `planta`, `codigo`, `estado`, `largo`, `anchura`, `tipo`, `longitud`, `latitud`) VALUES
(1, 2, 0, '1', 0, '5000.00', '2500.00', 'Normal', '-0.141207', '38.543446'),
(2, 2, 0, '2', 0, '5000.00', '2500.00', 'Normal', '-0.134407', '38.5421349'),
(3, 3, 0, '1', 0, '5000.00', '2500.00', 'Discapacitados', '-0.1341659', '38.5420472'),
(4, 3, 0, '2', 0, '5000.00', '2500.00', 'Eléctricos', '-0.1302675', '38.543847'),
(5, 2, 0, '3', 0, '5000.00', '2500.00', 'Normal', '-0.1343014', '38.5419485'),
(6, 2, 0, '4', 0, '5000.00', '2500.00', 'Normal', '-0.1336390', '38.542295');

--
-- Volcado de datos para la tabla `titularidadcoche`
--

INSERT INTO `titularidadcoche` (`idusuario`, `idvehiculo`) VALUES
(1, 1),
(1, 2),
(1, 15),
(3, 2),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(49, 17),
(50, 16);

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `dni`, `alias`, `nombre`, `apellidos`, `fechanac`, `direccion`, `numero`, `poblacion`, `provincia`, `codigopostal`, `tel`, `email`, `pass`, `isadmin`, `fechareg`) VALUES
(1, '48338052X', 'peluken90', 'Jesús', 'García Algarra', '1990-11-25 10:00:00', 'calle Arnedo', '6', 'Benidorm', 'Alicante', '03502', '647060281', 'jesusgarciaalgarra@gmail.com', 0x35393934343731616262303131313261666363313831353966366363373462346635313162393938303664613539623363616635613963313733636163666335, 1, '2021-04-10 18:50:34'),
(3, '44444444s', 'JuanitoElMejor', 'Juan', 'García Algarra', '1995-11-15 19:17:33', 'calle Arnedo', '6', 'Benidorm', 'Alicante', '03502', '646646646', 'juanito@gmail.com', 0x35393934343731616262303131313261666363313831353966366363373462346635313162393938303664613539623363616635613963313733636163666335, 0, '2021-04-11 18:26:11'),
(4, '32344344r', 'lady_di_17', 'Diana', 'Lavrysh', '2021-05-30 03:24:41', 'Calle Arnedo', '9', 'Benidorm', 'Alicante', '3502', '645084344', 'dlavrysh@gmail.com', 0x35393934343731616262303131313261666363313831353966366363373462346635313162393938303664613539623363616635613963313733636163666335, 0, '2021-04-21 19:08:53'),
(48, '11111111y', 'pablete', 'Pablo', 'Garcia Gamo', '1994-12-31 22:00:00', 'calle inventada', '2', 'Benidorm', 'Alicante', '3502', '645124344', 'pablete@gmail.com', 0x35393934343731616262303131313261666363313831353966366363373462346635313162393938303664613539623363616635613963313733636163666335, 1, '2021-05-29 16:57:13'),
(49, '22222222t', 'Alvarete', 'Alvaros', 'Sanchez', '1998-06-29 20:00:00', 'calle inventada 3', '2', 'Benidorm', 'Alicante', '3502', '232123212', 'alvarete@gmail.com', 0x35393934343731616262303131313261666363313831353966366363373462346635313162393938303664613539623363616635613963313733636163666335, 0, '2021-05-29 16:57:49'),
(50, '33333333r', 'joseko', 'Jose', 'Rodriguez', '1999-01-01 00:00:00', 'calle inventada', '3', 'Benidorm', 'Alicante', '3502', '645124344', 'josele@gmail.com', 0x35393934343731616262303131313261666363313831353966366363373462346635313162393938303664613539623363616635613963313733636163666335, 0, '2021-05-29 17:02:54');

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`idvehiculo`, `matricula`, `marca`, `modelo`, `carroceria`, `anyo`, `longitud`, `anchura`) VALUES
(1, 'a8640dw', 'opel', 'astra', 'sedan', '2020-12-31 21:00:00', '5000.00', '2500.00'),
(2, 'a8641dw', 'opel', 'corsa', 'urbano', '2007-02-08 23:00:00', '5000.00', '2500.00'),
(4, '1234avb', 'citroen', 'xsara', 'sedan', '2020-03-04 21:28:24', '5000.00', '2500.00'),
(5, '1212abc', 'peugeot', '307', 'cc', '2021-05-08 21:28:36', '5000.00', '2500.00'),
(6, '9890iii', 'jeep', '4x4', 'todoterreno', '2019-07-09 21:28:42', '5000.00', '2500.00'),
(7, '9898kkk', 'Nissan', 'almera', 'sedan', '2018-07-10 19:28:52', '5000.00', '2500.00'),
(8, '4658jhf', 'Mercedes', 'Benz', 'sedan', '2018-10-25 19:29:04', '5000.00', '2500.00'),
(15, '1234ssa', 'citroen', 'c4', 'coupe', '2021-04-29 20:00:00', '5000.00', '2500.00'),
(16, '4444ddd', 'opel', 'vectra', 'ranchera', '2021-05-28 00:00:00', '5000.00', '2500.00'),
(17, '3434ere', 'suzuki', 'vitara', 'todoterreno', '2021-05-01 00:00:00', '5000.00', '2500.00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
