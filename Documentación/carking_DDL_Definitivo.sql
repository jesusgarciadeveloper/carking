-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-06-2021 a las 21:44:47
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carking`
--
CREATE DATABASE IF NOT EXISTS `carking` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `carking`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE IF NOT EXISTS `articulo` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `idautor` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `contenido` varchar(10000) NOT NULL,
  `imgsrc` varchar(400) NOT NULL DEFAULT '../../../assets/fondo1.png',
  `fechapub` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idarticulo`),
  KEY `fk_idAutorArticulo_idUsuario` (`idautor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `idfaq` int(11) NOT NULL AUTO_INCREMENT,
  `idautor` int(11) NOT NULL,
  `pregunta` varchar(400) NOT NULL,
  `respuesta` varchar(4000) NOT NULL,
  PRIMARY KEY (`idfaq`),
  KEY `fk_idAutor_idUsuario` (`idautor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parking`
--

DROP TABLE IF EXISTS `parking`;
CREATE TABLE IF NOT EXISTS `parking` (
  `idparking` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `numero` varchar(3) DEFAULT NULL,
  `poblacion` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `codigopostal` varchar(5) NOT NULL,
  `tel` varchar(9) NOT NULL,
  `web` varchar(2048) NOT NULL,
  `correo` varchar(254) NOT NULL,
  `preciomin` decimal(3,2) DEFAULT NULL,
  `preciomedh` decimal(4,2) DEFAULT NULL,
  `precioh` decimal(4,2) DEFAULT NULL,
  `preciodia` decimal(4,2) DEFAULT NULL,
  `preciomaxd` decimal(4,2) DEFAULT NULL,
  `apertura` time NOT NULL,
  `cierre` time NOT NULL,
  `estadocierre` enum('Público','Cerrado') NOT NULL DEFAULT 'Cerrado',
  PRIMARY KEY (`idparking`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plaza`
--

DROP TABLE IF EXISTS `plaza`;
CREATE TABLE IF NOT EXISTS `plaza` (
  `idplaza` int(11) NOT NULL AUTO_INCREMENT,
  `idparking` int(11) NOT NULL,
  `planta` tinyint(1) DEFAULT 0,
  `codigo` varchar(4) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `largo` decimal(7,2) NOT NULL DEFAULT 5000.00,
  `anchura` decimal(7,2) NOT NULL DEFAULT 2500.00,
  `tipo` enum('Normal','Discapacitados','Eléctricos') NOT NULL DEFAULT 'Normal',
  `longitud` varchar(50) NOT NULL,
  `latitud` varchar(50) NOT NULL,
  PRIMARY KEY (`idplaza`),
  KEY `fk_idParkingPlaza_idParking` (`idparking`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

DROP TABLE IF EXISTS `reserva`;
CREATE TABLE IF NOT EXISTS `reserva` (
  `idusuario` int(11) NOT NULL,
  `idplaza` int(11) NOT NULL,
  `codigoreserva` varchar(20) NOT NULL,
  `diahora` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idusuario`,`idplaza`,`codigoreserva`),
  KEY `fk_idPlazaRes_idPlaza` (`idplaza`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `titularidadcoche`
--

DROP TABLE IF EXISTS `titularidadcoche`;
CREATE TABLE IF NOT EXISTS `titularidadcoche` (
  `idusuario` int(11) NOT NULL,
  `idvehiculo` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`,`idvehiculo`),
  KEY `fk_idVehiculo_idVehiculo` (`idvehiculo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(9) NOT NULL,
  `alias` varchar(15) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(120) NOT NULL,
  `fechanac` datetime DEFAULT NULL,
  `direccion` varchar(200) NOT NULL,
  `numero` varchar(3) DEFAULT NULL,
  `poblacion` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `codigopostal` varchar(5) NOT NULL,
  `tel` varchar(9) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `pass` longblob NOT NULL,
  `isadmin` tinyint(4) NOT NULL DEFAULT 0,
  `fechareg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `alias` (`alias`),
  UNIQUE KEY `dni` (`dni`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
CREATE TABLE IF NOT EXISTS `vehiculo` (
  `idvehiculo` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` varchar(7) NOT NULL,
  `marca` varchar(30) NOT NULL,
  `modelo` varchar(30) NOT NULL,
  `carroceria` varchar(30) NOT NULL,
  `anyo` datetime DEFAULT NULL,
  `longitud` decimal(7,2) NOT NULL DEFAULT 5000.00,
  `anchura` decimal(7,2) NOT NULL DEFAULT 2500.00,
  PRIMARY KEY (`idvehiculo`),
  UNIQUE KEY `matricula` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `fk_idAutorArticulo_idUsuario` FOREIGN KEY (`idAutor`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `fk_idAutor_idUsuario` FOREIGN KEY (`idAutor`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `plaza`
--
ALTER TABLE `plaza`
  ADD CONSTRAINT `fk_idParkingPlaza_idParking` FOREIGN KEY (`idParking`) REFERENCES `parking` (`idParking`);

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `fk_idPlazaRes_idPlaza` FOREIGN KEY (`idPlaza`) REFERENCES `plaza` (`idPlaza`),
  ADD CONSTRAINT `fk_idUsuarioRes_idUsuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `titularidadcoche`
--
ALTER TABLE `titularidadcoche`
  ADD CONSTRAINT `fk_idUsuarioVeh_idUsuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`),
  ADD CONSTRAINT `fk_idVehiculo_idVehiculo` FOREIGN KEY (`idVehiculo`) REFERENCES `vehiculo` (`idVehiculo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
