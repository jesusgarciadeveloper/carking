import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { INuevoParking } from '../interfaces/inuevo-parking';
import { IParkingmodificado } from '../interfaces/iparkingmodificado';

@Injectable({
  providedIn: 'root'
})
export class ParkingsService {
  private parkingsURL = 'parking/';

  constructor(private http: HttpClient) { }

  getParking(idParking: number): Observable<any> {
    const id = idParking.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.parkingsURL + "getparking", opts
    );
  }

  getParkingsId(idparking: number): Observable<any> {
    const id = idparking.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.parkingsURL + "id", opts
    );
  }

  getAllParkings(): Observable<any> {
    return this.http.get(environment.apiURL + this.parkingsURL);
  }

  eliminarParking(idparking: number): Observable<any> {
    const id = idparking.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.delete(
      environment.apiURL + this.parkingsURL, opts
    );
  }

  nuevoParking(parking: INuevoParking): Observable<any> {
    return this.http.post(environment.apiURL + this.parkingsURL, parking);
  }

  modificarParking(parking: IParkingmodificado): Observable<any> {
    return this.http.put(
      environment.apiURL + this.parkingsURL + 'update',
      parking
    );
  }
}
