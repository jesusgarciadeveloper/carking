import { Injectable } from '@angular/core';
import { IUsuario } from '../interfaces/iusuario';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { INuevoUsuario } from '../interfaces/inuevo-usuario';
import { IUsuarioModificado } from '../interfaces/iusuariomodificado';

@Injectable({
  providedIn: 'root',
})

export class UsuariosService {
  private usuariosURL = 'usuario/';

  constructor(private http: HttpClient) { }

  login(email: string, pass: string): Observable<any> {

    const datos = {
      "idWord": email,
      "pass": pass
    }

    return this.http.post(
      environment.apiURL + this.usuariosURL + 'login',
      datos
    );
  }

  logout(): null {
    this.http.get(environment.apiURL + this.usuariosURL + 'logout');
    sessionStorage.removeItem('usuario');
    document.cookie =
      'PHPSESSID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';
    return null;
  }

  isLogged(): boolean {
    if (sessionStorage.getItem('usuario')) {
      return true;
    }
    return false;
  }

  isAdmin(): boolean {

    return JSON.parse(sessionStorage.getItem('usuario')).isadmin;
  }

  getSesion(): any {
    return sessionStorage.getItem('usuario');
  }

  getUsuarios(): Observable<any> {
    return this.http.get(environment.apiURL + this.usuariosURL + "todosUsuarios");
  }

  getUsuario(id: string): Observable<any> {
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(environment.apiURL + this.usuariosURL, opts);
  }

  getUsuarioAlias(alias: string): Observable<any> {
    const opts = {
      params: new HttpParams({ fromObject: { alias } })
    };
    return this.http.get(environment.apiURL + this.usuariosURL + "alias", opts);
  }

  nuevoUsuario(usuario: INuevoUsuario): Observable<any> {
    return this.http.post(environment.apiURL + this.usuariosURL, usuario);
  }

  modificaUsuario(usuario: IUsuarioModificado): Observable<any> {
    return this.http.put(
      environment.apiURL + this.usuariosURL + 'update',
      usuario
    );
  }

  eliminarUsuario(idusuario: number): Observable<any> {
    const id = idusuario.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.delete(
      environment.apiURL + this.usuariosURL, opts
    );
  }
}
