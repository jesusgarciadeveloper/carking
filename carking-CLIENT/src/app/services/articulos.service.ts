import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Iarticulo } from '../interfaces/iarticulo';
import { InuevoArticulo } from '../interfaces/inuevo-articulo';
import { Iarticulomodificado } from '../interfaces/iarticulomodificado';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {
  private articulosURL = 'articulo/';

  constructor(private http: HttpClient) { }

  getArticulo(idarticulo: number): Observable<any> {
    const id = idarticulo.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.articulosURL + "detalle", opts
    );
  }

  getArticulosId(idarticulo: number): Observable<any> {
    const id = idarticulo.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.articulosURL + "id", opts
    );
  }

  getAllArticulos(): Observable<any> {
    return this.http.get(environment.apiURL + this.articulosURL);
  }

  getLastArticulos(): Observable<any> {
    return this.http.get(environment.apiURL + this.articulosURL + "last");
  }

  nuevoArticulo(articulo: InuevoArticulo): Observable<any> {
    return this.http.post(environment.apiURL + this.articulosURL, articulo);
  }

  modificaArticulo(articulo: Iarticulomodificado): Observable<any> {
    return this.http.put(
      environment.apiURL + this.articulosURL + 'update',
      articulo
    );
  }

  eliminarArticulo(idarticulo: number): Observable<any> {
    const id = idarticulo.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.delete(
      environment.apiURL + this.articulosURL, opts
    );
  }
}
