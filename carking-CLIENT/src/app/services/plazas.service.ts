import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { INuevaPlaza } from '../interfaces/inueva-plaza';
import { IPlazamodificada } from '../interfaces/iplazamodificada';

@Injectable({
  providedIn: 'root'
})
export class PlazasService {

  private plazasURL = 'plaza/';

  constructor(private http: HttpClient) { }

  getAllPlazas(): Observable<any> {
    return this.http.get(environment.apiURL + this.plazasURL);
  }

  getAllReservas(): Observable<any> {
    return this.http.get(environment.apiURL + this.plazasURL + 'todasreservas');
  }

  reservarPlaza(idpla: number, idusu: number, codigo: string): Observable<any> {
    const idusuario = idusu.toString();
    const idplaza = idpla.toString();

    return this.http.put(environment.apiURL + this.plazasURL + "reservar", { "idplaza": idplaza, "idusuario": idusuario, "codigo": codigo });
  }

  cancelarReserva(idpla: number, codigo: string, estado: number): Observable<any> {

    const estad = estado.toString();
    const idplaza = idpla.toString();

    return this.http.put(environment.apiURL + this.plazasURL + "cancelar", { "idplaza": idplaza, "codigoreserva": codigo, "estado": estad });
  }

  getReservas(idusu: number): Observable<any> {
    const idusuario = idusu.toString();
    const opts = {
      params: new HttpParams({ fromObject: { idusuario } })
    };
    return this.http.get(environment.apiURL + this.plazasURL + "reservadeusuario", opts);
  }

  getPlazasId(idplaza: number): Observable<any> {
    const id = idplaza.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.plazasURL + "id", opts
    );
  }

  getPlazasIdParking(idparking: number): Observable<any> {
    const id = idparking.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.plazasURL + "idParking", opts
    );
  }

  eliminarPlaza(idplaza: number): Observable<any> {
    const id = idplaza.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.delete(
      environment.apiURL + this.plazasURL, opts
    );
  }

  insertarPlaza(plaza: INuevaPlaza): Observable<any> {
    return this.http.post(environment.apiURL + this.plazasURL, plaza);
  }

  modificarPlaza(plaza: IPlazamodificada): Observable<any> {
    return this.http.put(
      environment.apiURL + this.plazasURL + 'update',
      plaza
    );
  }

}
