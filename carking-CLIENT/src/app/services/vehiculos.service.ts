import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { INuevoVehiculo } from '../interfaces/inuevo-vehiculo';
import { IVehiculo } from '../interfaces/ivehiculo';
import { IVehiculomodificado } from '../interfaces/ivehiculomodificado';

@Injectable({
  providedIn: 'root'
})
export class VehiculosService {
  vehiculosURL = "vehiculo/";

  constructor(private http: HttpClient) { }

  getCarsFromUser(idusuario: number): Observable<any> {
    const id = idusuario.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.vehiculosURL + "usuario", opts
    );
  }

  existeMatricula(matricula: string): Observable<any> {

    const opts = {
      params: new HttpParams({ fromObject: { matricula } })
    };
    return this.http.get(
      environment.apiURL + this.vehiculosURL + "matricula", opts
    );
  }

  eliminarVehiculo(idvehiculo: number, idusu: number): Observable<any> {
    const idcar = idvehiculo.toString();
    const idusuario = idusu.toString();
    const opts = {
      params: new HttpParams({ fromObject: { idcar, idusuario } })
    };
    return this.http.delete(
      environment.apiURL + this.vehiculosURL, opts
    );
  }

  insertVehiculo(vehiculo: INuevoVehiculo, idusu: number): Observable<any> {
    const idusuario = idusu.toString();
    const opts = {
      params: new HttpParams({ fromObject: { idusuario } })
    };

    return this.http.post(
      environment.apiURL + this.vehiculosURL, vehiculo, opts
    );
  }

  modificaVehiculo(vehiculo: IVehiculomodificado): Observable<any> {
    return this.http.put(
      environment.apiURL + this.vehiculosURL + 'update',
      vehiculo
    );
  }


}
