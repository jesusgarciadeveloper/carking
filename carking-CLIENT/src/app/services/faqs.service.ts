import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Ifaq } from '../interfaces/ifaq';
import { InuevaFaq } from '../interfaces/inueva-faq';
import { Ifaqmodificada } from '../interfaces/ifaqmodificada';

@Injectable({
  providedIn: 'root'
})
export class FaqsService {
  private faqsURL = 'faq/';

  constructor(private http: HttpClient) { }

  getAllFaqs(): Observable<any> {
    return this.http.get(environment.apiURL + this.faqsURL);
  }

  getFaqsId(idfaq: number): Observable<any> {
    const id = idfaq.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.get(
      environment.apiURL + this.faqsURL + "id", opts
    );
  }

  nuevaFaq(faq: InuevaFaq): Observable<any> {
    return this.http.post(environment.apiURL + this.faqsURL, faq);
  }

  modificaFaq(faq: Ifaqmodificada): Observable<any> {
    return this.http.put(
      environment.apiURL + this.faqsURL + 'update',
      faq
    );
  }

  eliminarFaq(idfaq: number): Observable<any> {
    const id = idfaq.toString();
    const opts = {
      params: new HttpParams({ fromObject: { id } })
    };
    return this.http.delete(
      environment.apiURL + this.faqsURL, opts
    );
  }
}
