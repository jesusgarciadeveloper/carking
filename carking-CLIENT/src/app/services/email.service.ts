import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IEmail } from '../interfaces/iemail';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(
    private http: HttpClient
  ) {

  }

  sendEmail(email: IEmail): Observable<any> {
    return this.http.post(environment.apiURL + 'enviar', email);
  }
}

