import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { INuevoParking } from 'src/app/interfaces/inuevo-parking';
import { IParking } from 'src/app/interfaces/iparking';
import { ParkingsService } from 'src/app/services/parkings.service';

@Component({
  selector: 'app-add-parking',
  templateUrl: './add-parking.component.html',
  styleUrls: ['./add-parking.component.css']
})
export class AddParkingComponent implements OnInit {
  parking: INuevoParking;

  constructor(private router: Router, private parkingsService: ParkingsService
  ) { }

  ngOnInit(): void {
    this.parking = {
      nombre: '',
      direccion: '',
      numero: '',
      poblacion: '',
      provincia: '',
      codigopostal: '',
      tel: '',
      web: '',
      correo: '',
      preciomin: 0.00,
      preciomedh: 0.00,
      precioh: 0.00,
      preciodia: 0.00,
      preciomaxd: 0.00,
      apertura: '',
      cierre: '',
      estadocierre: 'Cerrado',
    };
  }

  cambioFecha($event) {
    let d = new Date();
    d.setHours($event.target.value.split(':')[0], $event.target.value.split(':')[1], 0);

    if ($event.target.id == "aperturaModel") {
      this.parking.apertura = d.toUTCString();
    } else {
      this.parking.cierre = d.toUTCString();
    }
  }

  registro(): void {

    this.parkingsService.nuevoParking(this.parking).subscribe(
      (res) => {
        alert("Parking insertado correctamente");
        this.router.navigate(['/admin/parkings']);
      }, (error) => alert(error.error.message)
    );
  }

}
