import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InuevaFaq } from 'src/app/interfaces/inueva-faq';
import { FaqsService } from 'src/app/services/faqs.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.css']
})
export class AddFaqComponent implements OnInit {
  faq: InuevaFaq = {
    "idautor": 0,
    "pregunta": "",
    "respuesta": ""
  }
  constructor(private router: Router, private faqsService: FaqsService, private usuariosService: UsuariosService) { }

  ngOnInit(): void {
  }

  registro(): void {
    let usuario = JSON.parse(this.usuariosService.getSesion());

    this.faq.idautor = usuario.idusuario;

    this.faqsService.nuevaFaq(this.faq).subscribe(
      (res) => {
        alert("Faq insertada correctamente");
        this.router.navigate(['/admin/faqs']);
      }, (error) => alert(error.error.message)
    );
  }

}
