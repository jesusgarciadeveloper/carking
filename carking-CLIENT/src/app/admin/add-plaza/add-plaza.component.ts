import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { INuevaPlaza } from 'src/app/interfaces/inueva-plaza';
import { IParking } from 'src/app/interfaces/iparking';
import { ParkingsService } from 'src/app/services/parkings.service';
import { PlazasService } from 'src/app/services/plazas.service';

@Component({
  selector: 'app-add-plaza',
  templateUrl: './add-plaza.component.html',
  styleUrls: ['./add-plaza.component.css']
})
export class AddPlazaComponent implements OnInit {

  plazas: Array<INuevaPlaza>;
  parkings: Array<IParking>;
  idparking: number;
  planta: number;
  codigo: string;
  longitud: number;
  latitud: number;
  tipoPlaza: string;
  numPlazas: number;

  constructor(
    private plazasService: PlazasService,
    private parkingsService: ParkingsService,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.idparking = 0;
    this.numPlazas = 1;
    this.longitud = 0;
    this.latitud = 0;
    this.planta = 0;
    this.tipoPlaza = "Normal";
    this.codigo = "1";
    this.parkings = [];
    this.parkingsService.getAllParkings().subscribe(
      (res) => {
        this.parkings = res;
      }, (error) => alert(error.error.message)
    );
    this.plazas = [];
  }

  registro() {
    if (this.idparking == 0) {
      alert("El parking es imprescindible");
    } else {

      let plaza: INuevaPlaza;

      if (this.codigo.trim() == "1") {
        for (let index = 0; index < this.numPlazas; index++) {
          plaza = {
            "idparking": this.idparking,
            "codigo": (index + 1).toString(),
            "planta": this.planta,
            "tipo": this.tipoPlaza,
            "longitud": this.longitud.toString(),
            "latitud": this.latitud.toString()
          }
          this.plazasService.insertarPlaza(plaza).subscribe(
            (res) => {
              this.plazas.push(res);
            }, (error) => {
              alert(error.error.message);
            }
          );
        }

      } else {
        for (let index = 0; index < this.numPlazas; index++) {
          plaza = {
            "idparking": this.idparking,
            "codigo": this.codigo,
            "planta": this.planta,
            "tipo": this.tipoPlaza,
            "longitud": this.longitud.toString(),
            "latitud": this.latitud.toString()
          }
          this.plazasService.insertarPlaza(plaza).subscribe(
            (res) => {
              this.plazas.push(res)

            }, (error) => {
              alert(error.error.message);
            }
          );
        }
      }


      setTimeout(() => {
        if (this.plazas.length > 0) {
          alert("Plaza/s insertadas correctamente");
          this.router.navigate(["/admin/plazas"]);
        }
      }, 500);
    }
  }

}
