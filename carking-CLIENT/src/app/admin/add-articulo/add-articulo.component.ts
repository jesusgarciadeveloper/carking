import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InuevoArticulo } from 'src/app/interfaces/inuevo-articulo';
import { ArticulosService } from 'src/app/services/articulos.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-add-articulo',
  templateUrl: './add-articulo.component.html',
  styleUrls: ['./add-articulo.component.css']
})
export class AddArticuloComponent implements OnInit {
  articulo: InuevoArticulo = {
    "idautor": 0,
    "titulo": "",
    "contenido": "",
    "imgsrc": ""
  }
  constructor(private router: Router, private articulosService: ArticulosService, private usuariosService: UsuariosService) { }

  ngOnInit(): void {
  }

  registro(): void {
    let usuario = JSON.parse(this.usuariosService.getSesion());

    this.articulo.idautor = usuario.idusuario;

    this.articulosService.nuevoArticulo(this.articulo).subscribe(
      (res) => {
        alert("Artículo insertado correctamente");
        this.router.navigate(['/admin/articulos']);
      }, (error) => alert(error.error.message)
    );
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      let base64 = reader.result as string;

      /* base64 = base64.split(',')[1]; */
      this.articulo.imgsrc = base64;
    });
  }

}
