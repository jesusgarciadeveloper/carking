import { Component, OnInit } from '@angular/core';
import { FaqsService } from 'src/app/services/faqs.service';

@Component({
  selector: 'app-admin-faqs',
  templateUrl: './admin-faqs.component.html',
  styleUrls: ['./admin-faqs.component.css']
})
export class AdminFaqsComponent implements OnInit {
  campoBusqueda: string;
  resultados: boolean;
  errorId: boolean;
  cargando: boolean;
  noresultados: boolean;
  faqs: any[];
  public page: number;
  constructor(private faqsService: FaqsService) { }

  ngOnInit(): void {
    this.campoBusqueda = "";
    this.resultados = false;
    this.errorId = false;
    this.cargando = true;
    this.noresultados = false;
    this.faqs = [];
    this.faqsService.getAllFaqs().subscribe(
      (res) => {

        this.faqs = res;
      }, (error) => alert(error.error.message)
    );
    this.cargando = false;
  }

  busqueda(valor) {
    if (isNaN(valor) || valor.length > 0 && valor.trim() == "") {

      this.errorId = true;
    } else {
      this.errorId = false;

      if (valor.length > 0) {
        this.faqsService.getFaqsId(valor).subscribe(
          (res) => {

            this.cargando = true;

            this.faqs = res;
            if (this.faqs.length < 1 && valor.length >= 1) {
              this.noresultados = true;
            } else {
              this.noresultados = false;
            }
            this.cargando = false;
            this.resultados = true;
          }
        );
      } else {
        this.campoBusqueda = "";
        this.cargando = true;
        this.noresultados = false;
        this.faqs = [];
        this.faqsService.getAllFaqs().subscribe(
          (res) => {

            this.faqs = res;
          }, (error) => alert(error.error.message)
        );
        this.cargando = false;
      }
    }
  }
  eliminar(id) {
    this.faqsService.eliminarFaq(id).subscribe(
      (res) => {
        alert("FAQ eliminada correctamente");
        window.location.reload();
      }, (error) => alert(error.error.message)
    );
  }


}
