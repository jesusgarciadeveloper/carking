import { Component, OnInit } from '@angular/core';
import { ParkingsService } from 'src/app/services/parkings.service';

@Component({
  selector: 'app-admin-parkings',
  templateUrl: './admin-parkings.component.html',
  styleUrls: ['./admin-parkings.component.css']
})
export class AdminParkingsComponent implements OnInit {
  campoBusqueda: string;
  resultados: boolean;
  cargando: boolean;
  errorId: boolean;
  noresultados: boolean;
  parkings: any[];

  provincia: string;
  poblacion: string;
  direccion: string;
  public page: number;



  constructor(private parkingsService: ParkingsService) { }

  ngOnInit(): void {
    this.parkings = [];
    this.campoBusqueda = "";
    this.errorId = false;
    this.resultados = false;
    this.cargando = true;
    this.noresultados = false;
    this.parkingsService.getAllParkings().subscribe(
      (res) => {

        this.parkings = res;
      }, (error) => alert(error.error.message)
    );
    this.cargando = false;
  }

  busqueda(valor) {
    let valorNum = parseInt(valor);

    if (isNaN(valor) || valor.length > 0 && valor.trim() == "") {

      this.errorId = true;
    } else {
      this.errorId = false;
      if (valor.length > 0) {
        this.parkingsService.getParkingsId(valor).subscribe(
          (res) => {

            this.cargando = true;

            this.parkings = res;
            if (this.parkings.length < 1 && valor.length >= 1) {
              this.noresultados = true;
            } else {
              this.noresultados = false;
            }
            this.cargando = false;
            this.resultados = true;
          }
        );
      } else {
        this.campoBusqueda = "";
        this.cargando = true;
        this.noresultados = false;
        this.parkings = [];
        this.parkingsService.getAllParkings().subscribe(
          (res) => {

            this.parkings = res;
          }, (error) => alert(error.error.message)
        );
        this.cargando = false;
      }
    }
  }

  eliminar(id) {
    this.parkingsService.eliminarParking(id).subscribe(
      (res) => {
        alert("Parking eliminado correctamente");
        window.location.reload();
      }, (error) => console.log(error)
    );
  }


}
