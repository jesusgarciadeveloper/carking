import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Iarticulo } from 'src/app/interfaces/iarticulo';
import { Iarticulomodificado } from 'src/app/interfaces/iarticulomodificado';
import { ArticulosService } from 'src/app/services/articulos.service';

@Component({
  selector: 'app-editar-articulo',
  templateUrl: './editar-articulo.component.html',
  styleUrls: ['./editar-articulo.component.css']
})
export class EditarArticuloComponent implements OnInit {
  articulo: Iarticulo = {
    "idarticulo": 0,
    "idautor": 0,
    "titulo": "",
    "contenido": "",
    "imgsrc": "",
    "fechapub": ""
  }

  constructor(
    private routerService: Router,
    private route: ActivatedRoute,
    private articulosService: ArticulosService
  ) { }

  ngOnInit(): void {
    let idart = this.route.snapshot.params.id;
    this.articulosService.getArticulosId(idart).subscribe(
      (res) => {
        res.forEach(element => {
          element.fechapub = new Date(element.fechapub.date);
        });
        this.articulo = res[0];

      });
  }

  registro(): void {
    this.articulosService.modificaArticulo(this.articulo).subscribe(
      (res) => {
        alert("Artículo modificado exitósamente");

        window.location.reload();
      }, (error) => alert(error.error.message)

    );
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.articulo.imgsrc = reader.result as string;
    });
  }
}
