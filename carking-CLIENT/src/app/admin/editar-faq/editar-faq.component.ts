import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ifaq } from 'src/app/interfaces/ifaq';
import { FaqsService } from 'src/app/services/faqs.service';

@Component({
  selector: 'app-editar-faq',
  templateUrl: './editar-faq.component.html',
  styleUrls: ['./editar-faq.component.css']
})
export class EditarFaqComponent implements OnInit {

  faq: Ifaq = {
    "idfaq": 0,
    "idautor": 0,
    "pregunta": "",
    "respuesta": ""
  }
  constructor(
    private routerService: Router,
    private route: ActivatedRoute,
    private faqsService: FaqsService
  ) { }

  ngOnInit(): void {
    let idfaq = this.route.snapshot.params.id;

    this.faqsService.getFaqsId(idfaq).subscribe(
      (res) => {
        this.faq = res[0];
      }, (error) => alert(error.error.message)

    )
  }

  registro(): void {
    this.faqsService.modificaFaq(this.faq).subscribe(
      (res) => {
        alert("Faq modificada exitósamente");

        window.location.reload();
      }, (error) => alert(error.error.message)

    );
  }

}
