import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarFaqComponent } from './editar-faq.component';

describe('EditarFaqComponent', () => {
  let component: EditarFaqComponent;
  let fixture: ComponentFixture<EditarFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarFaqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
