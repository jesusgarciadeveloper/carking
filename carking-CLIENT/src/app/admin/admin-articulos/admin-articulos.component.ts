import { Component, OnInit } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos.service';

@Component({
  selector: 'app-admin-articulos',
  templateUrl: './admin-articulos.component.html',
  styleUrls: ['./admin-articulos.component.css']
})
export class AdminArticulosComponent implements OnInit {
  campoBusqueda: string;
  resultados: boolean;
  errorId: boolean;
  cargando: boolean;
  noresultados: boolean;
  articulos: any[];
  public page: number;
  constructor(private articulosService: ArticulosService) { }

  ngOnInit(): void {
    this.campoBusqueda = "";
    this.resultados = false;
    this.errorId = false;
    this.cargando = true;
    this.noresultados = false;
    this.articulos = [];
    this.articulosService.getAllArticulos().subscribe(
      (res) => {
        res.forEach(element => {
          element.fechapub = new Date(element.fechapub.date);
        });
        this.articulos = res;
      }, (error) => alert(error.error.message)
    );
    this.cargando = false;
  }

  busqueda(valor) {
    if (isNaN(valor) || valor.length > 0 && valor.trim() == "") {

      this.errorId = true;
    } else {
      this.errorId = false;
      if (valor.length > 0) {
        this.articulosService.getArticulosId(valor).subscribe(
          (res) => {

            this.cargando = true;
            res.forEach(element => {
              element.fechapub = new Date(element.fechapub.date);
            });
            this.articulos = res;
            if (this.articulos.length < 1 && valor.length >= 1) {
              this.noresultados = true;
            } else {
              this.noresultados = false;
            }
            this.cargando = false;
            this.resultados = true;
          }
        );
      } else {
        this.campoBusqueda = "";
        this.cargando = true;
        this.noresultados = false;
        this.articulos = [];
        this.articulosService.getAllArticulos().subscribe(
          (res) => {
            res.forEach(element => {
              element.fechapub = new Date(element.fechapub.date);
            });
            this.articulos = res;
          }, (error) => alert(error.error.message)
        );
        this.cargando = false;
      }
    }
  }

  eliminar(id) {
    this.articulosService.eliminarArticulo(id).subscribe(
      (res) => {
        alert("Artículo eliminado correctamente");
        window.location.reload();
      }, (error) => alert(error.error.message)
    );
  }



}
