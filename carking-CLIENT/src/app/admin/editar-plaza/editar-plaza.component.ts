import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPlazamodificada } from 'src/app/interfaces/iplazamodificada';
import { PlazasService } from 'src/app/services/plazas.service';

@Component({
  selector: 'app-editar-plaza',
  templateUrl: './editar-plaza.component.html',
  styleUrls: ['./editar-plaza.component.css']
})
export class EditarPlazaComponent implements OnInit {

  plaza: IPlazamodificada;



  constructor(
    private routerService: Router,
    private route: ActivatedRoute,
    private plazasService: PlazasService
  ) { }

  ngOnInit(): void {
    this.plaza = {
      'idplaza': 0,
      'idparking': 0,
      "planta": 0,
      'codigo': "",
      'estado': 0,
      'tipo': "",
      'longitud': "",
      'latitud': ""
    }
    let idplaza = this.route.snapshot.params.id;
    this.plazasService.getPlazasId(idplaza).subscribe(
      (res) => {

        this.plaza = res[0];
      }, (error) => alert(error.error.message)

    );
  }

  registro(): void {
    this.plazasService.modificarPlaza(this.plaza).subscribe(
      (res) => {
        alert("Plaza modificada exitósamente");

        window.location.reload();
      }, (error) => alert(error.error.message)

    );
  }

}
