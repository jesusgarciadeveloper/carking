import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarPlazaComponent } from './editar-plaza.component';

describe('EditarPlazaComponent', () => {
  let component: EditarPlazaComponent;
  let fixture: ComponentFixture<EditarPlazaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarPlazaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarPlazaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
