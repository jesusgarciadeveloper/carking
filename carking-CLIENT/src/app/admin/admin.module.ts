import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UsuariosService } from '../services/usuarios.service';

import { AdminUsuariosComponent } from './admin-usuarios/admin-usuarios.component';
import { AdminComponent } from './admin/admin.component';
import { AdminNavbarComponent } from './admin-navbar/admin-navbar.component';
import { AdminParkingsComponent } from './admin-parkings/admin-parkings.component';
import { AdminPlazasComponent } from './admin-plazas/admin-plazas.component';
import { AdminFaqsComponent } from './admin-faqs/admin-faqs.component';
import { AdminArticulosComponent } from './admin-articulos/admin-articulos.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { EditarParkingComponent } from './editar-parking/editar-parking.component';
import { EditarPlazaComponent } from './editar-plaza/editar-plaza.component';
import { EditarArticuloComponent } from './editar-articulo/editar-articulo.component';
import { EditarFaqComponent } from './editar-faq/editar-faq.component';
import { AddUsuarioComponent } from './add-usuario/add-usuario.component';
import { AddParkingComponent } from './add-parking/add-parking.component';
import { AddPlazaComponent } from './add-plaza/add-plaza.component';
import { AddArticuloComponent } from './add-articulo/add-articulo.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { VehiculoComponent } from 'src/app/components/vehiculo/vehiculo.component';

@NgModule({
  declarations: [
    AdminUsuariosComponent,
    AdminComponent,
    AdminNavbarComponent,
    AdminParkingsComponent,
    AdminPlazasComponent,
    AdminFaqsComponent,
    AdminArticulosComponent,
    EditarUsuarioComponent,
    EditarParkingComponent,
    EditarPlazaComponent,
    EditarArticuloComponent,
    EditarFaqComponent,
    AddUsuarioComponent,
    AddParkingComponent,
    AddPlazaComponent,
    AddArticuloComponent,
    AddFaqComponent
  ],
  imports: [
    NgxPaginationModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [UsuariosService],
  exports: [AdminComponent, AdminUsuariosComponent]
})
export class AdminModule { }
