import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { INuevoUsuario } from 'src/app/interfaces/inuevo-usuario';
import { INuevoVehiculo } from 'src/app/interfaces/inuevo-vehiculo';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { VehiculosService } from 'src/app/services/vehiculos.service';
import { VehiculoComponent } from 'src/app/components/vehiculo/vehiculo.component';


@Component({
  selector: 'app-add-usuario',
  templateUrl: './add-usuario.component.html',
  styleUrls: ['./add-usuario.component.css']
})
export class AddUsuarioComponent implements OnInit {
  usuario: INuevoUsuario;
  fechaAnyo: string;
  vehiculo: INuevoVehiculo;
  isadmin: any;

  constructor(
    private usuariosService: UsuariosService,
    private routerService: Router,
    private vehiculosService: VehiculosService
  ) { }

  ngOnInit(): void {
    this.vehiculo = {
      'matricula': '',
      'marca': '',
      'modelo': '',
      'carroceria': '',
      'anyo': ''
    }

    this.usuario = {
      tel: '',
      nombre: '',
      apellidos: '',
      alias: '',
      dni: '',
      fechanac: '',
      direccion: '',
      numero: '',
      codigopostal: '',
      poblacion: '',
      provincia: '',
      email: '',
      pass: '',
      isadmin: false,
    };

  }

  registro(): void {
    this.usuariosService.nuevoUsuario(this.usuario).subscribe(
      (result) => {
        let usuario = result;
        alert("Usuario creado exitósamente");
        if (this.vehiculo.matricula != "") {
          /* COMPROBAR AQUÍ SI YA EXISTE EL VEHÍCULO */

          this.vehiculo.anyo = this.fechaAnyo;
          this.vehiculosService.insertVehiculo(this.vehiculo, usuario.idusuario).subscribe(
            (res) => {

              alert("Vehículo creado exitósamente");

            }, (error) => alert(error.error.message)

          );
        }

        this.routerService.navigate(['/admin/usuarios']);

      }, (error) => alert(error.error.message)


    );

  }

  generarPass(num): void {

    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_';
    let result1 = '';
    const charactersLength = characters.length;
    for (let i = 0; i < num; i++) {
      result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    this.usuario.pass = result1;
  }

  admin(event): void {
    if (event.target.checked) {
      this.usuario.isadmin = true;
    } else {
      this.usuario.isadmin = false;
    }
  }

  cambioFecha($event) {
    this.fechaAnyo = new Date($event.target.value).toUTCString();
  }


}
