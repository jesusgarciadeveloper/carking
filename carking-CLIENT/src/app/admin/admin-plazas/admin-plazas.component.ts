import { Component, OnInit } from '@angular/core';
import { PlazasService } from 'src/app/services/plazas.service';
@Component({
  selector: 'app-admin-plazas',
  templateUrl: './admin-plazas.component.html',
  styleUrls: ['./admin-plazas.component.css']
})
export class AdminPlazasComponent implements OnInit {
  plazas: any[];
  campoBusqueda: string;
  resultados: boolean;
  cargando: boolean;
  errorId: boolean;
  noresultados: boolean;


  provincia: string;
  poblacion: string;
  direccion: string;
  public page: number;

  constructor(private plazasService: PlazasService) { }


  ngOnInit(): void {
    this.plazas = [];
    this.campoBusqueda = "";
    this.errorId = false;
    this.resultados = false;
    this.cargando = true;
    this.noresultados = false;
    this.plazasService.getAllPlazas().subscribe(
      (res) => {

        this.plazas = res;
      }, (error) => alert(error.error.message)
    );
    this.cargando = false;
  }

  busqueda(valor) {
    let valorNum = parseInt(valor);

    if (isNaN(valor) || valor.length > 0 && valor.trim() == "") {

      this.errorId = true;
    } else {
      this.errorId = false;
      if (valor.length > 0) {
        this.plazasService.getPlazasIdParking(valor).subscribe(
          (res) => {

            this.cargando = true;

            this.plazas = res;
            if (this.plazas.length < 1 && valor.length >= 1) {
              this.noresultados = true;
            } else {
              this.noresultados = false;
            }
            this.cargando = false;
            this.resultados = true;
          }
        );
      } else {
        this.campoBusqueda = "";
        this.cargando = true;
        this.noresultados = false;
        this.plazas = [];
        this.plazasService.getAllPlazas().subscribe(
          (res) => {

            this.plazas = res;
          }, (error) => alert(error.error.message)
        );
        this.cargando = false;
      }
    }
  }

  eliminar(id) {
    this.plazasService.eliminarPlaza(id).subscribe(
      (res) => {
        alert("Plaza eliminada correctamente");
        window.location.reload();
      }, (error) => alert(error.error.message)
    );
  }



}
