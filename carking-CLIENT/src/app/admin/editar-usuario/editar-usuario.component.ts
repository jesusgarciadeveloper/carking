import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { ActivatedRoute } from '@angular/router';
import { IVehiculo } from 'src/app/interfaces/ivehiculo';
import { VehiculosService } from 'src/app/services/vehiculos.service';
import { IUsuarioModificado } from 'src/app/interfaces/iusuariomodificado';
import { INuevoVehiculo } from 'src/app/interfaces/inuevo-vehiculo';


@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {
  usuario: IUsuarioModificado;
  isadmin: any;
  fechaNac: string;
  vehiculos: Array<IVehiculo>;

  constructor(
    private usuariosService: UsuariosService,
    private vehiculosService: VehiculosService,
    private routerService: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.usuario = {
      idusuario: 0,
      tel: '',
      nombre: '',
      apellidos: '',
      alias: '',
      dni: '',
      fechanac: '',
      direccion: '',
      numero: '',
      codigopostal: '',
      poblacion: '',
      provincia: '',
      email: '',
      pass: '',
      isadmin: false,
    };
    this.vehiculos = [];
    let idusu = this.route.snapshot.params.id;
    this.usuariosService.getUsuario(idusu).subscribe(
      (res) => {
        res.fechanac = new Date(res.fechanac);
        res.pass = "";

        this.usuario = res;
        this.fechaNac = this.usuario.fechanac;
      }, (error) => alert(error.error.message)

    );
    this.vehiculosService.getCarsFromUser(idusu).subscribe(
      (res) => {
        res.forEach(element => {
          element.anyo = new Date(element.anyo.date);
        });
        this.vehiculos = res;

      }, (error) => alert(error.error.message)

    );

  }

  cambioFecha($event) {
    this.fechaNac = new Date($event.target.value).toUTCString();
  }

  cambioFechaVeh($event, id) {
    let coche = this.vehiculos.find(element => element.idvehiculo == id);
    let posicion = this.vehiculos.indexOf(coche);
    let fechaAnyoVehiculo = new Date($event.target.value).toUTCString();
    coche.anyo = fechaAnyoVehiculo;
    this.vehiculos[posicion] = coche;

  }

  eliminarVehiculo(vehiculo: IVehiculo) {
    let coche = this.vehiculos.find(element => element.idvehiculo == vehiculo.idvehiculo);
    let posicion = this.vehiculos.indexOf(coche);

    if (posicion !== -1) {
      this.vehiculos.splice(posicion, 1);
    }
  }

  registro(): void {
    this.usuario.fechanac = this.fechaNac;
    this.usuariosService.modificaUsuario(this.usuario).subscribe(
      (result) => {
        this.vehiculos.forEach(element => {
          if (element.idvehiculo == 0 && element.matricula != "") {

            this.vehiculosService.insertVehiculo(element, this.usuario.idusuario).subscribe(
              (res) => {

                res.anyo = new Date(res.anyo.date);
              }, (error) => alert(error.error.message)

            );
          } else {

            this.vehiculosService.modificaVehiculo(element).subscribe(

              (res) => {
                res.anyo = new Date(res.anyo.date);
              }, (error) => alert(error.error.message)

            );
          }
        });
        alert("Usuario modificado exitósamente");
        window.location.reload();

      }, (error) => alert(error.error.message)

    );

  }

  generarPass(num): void {

    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_';
    let result1 = '';
    const charactersLength = characters.length;
    for (let i = 0; i < num; i++) {
      result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    this.usuario.pass = result1;
  }

  admin(event): void {
    if (event.target.checked) {
      this.usuario.isadmin = true;
    } else {
      this.usuario.isadmin = false;
    }
  }

  addcar() {
    let veh: IVehiculo = {
      idvehiculo: 0,
      matricula: "",
      marca: "",
      modelo: "",
      carroceria: "",
      anyo: ""
    }
    this.vehiculos.push(veh);
  }
}
