import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-admin-usuarios',
  templateUrl: './admin-usuarios.component.html',
  styleUrls: ['./admin-usuarios.component.css']
})
export class AdminUsuariosComponent implements OnInit {
  usuarios: any[];
  campoBusqueda: string;
  resultados: boolean;
  cargando: boolean;
  noresultados: boolean;
  public page: number;

  constructor(
    private usuariosService: UsuariosService
  ) { }

  ngOnInit(): void {
    this.campoBusqueda = "";
    this.resultados = false;
    this.cargando = true;
    this.noresultados = false;
    this.usuarios = [];
    this.usuariosService.getUsuarios().subscribe(
      (res) => {
        this.usuarios = res;
      }, (error) => alert(error.error.message)
    );
    this.cargando = false;
  }

  busqueda(valor) {

    if (valor.length > 0) {
      this.usuariosService.getUsuarioAlias(valor).subscribe(
        (res) => {

          this.cargando = true;

          this.usuarios = res;
          if (this.usuarios.length < 1 && valor.length >= 1) {
            this.noresultados = true;
          } else {
            this.noresultados = false;
          }
          this.cargando = false;
          this.resultados = true;
        }
      );
    } else {
      this.campoBusqueda = "";
      this.cargando = true;
      this.noresultados = false;
      this.usuarios = [];
      this.usuariosService.getUsuarios().subscribe(
        (res) => {
          this.usuarios = res;
        }, (error) => alert(error.error.message)
      );
      this.cargando = false;
    }
  }


  eliminar(id) {
    let usuarioLogueado = JSON.parse(this.usuariosService.getSesion());
    if (usuarioLogueado.idusuario == id) {
      alert("Para eliminar tu cuenta propia dirígete a la ventana de modificar tu perfil a través de la parte pública.")
      return false;

    }
    this.usuariosService.eliminarUsuario(id).subscribe(
      (res) => {
        alert("Usuario eliminado correctamente");
        window.location.reload();
      }, (error) => alert(error.error.message)
    );
  }

}
