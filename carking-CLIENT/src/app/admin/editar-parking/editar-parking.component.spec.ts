import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarParkingComponent } from './editar-parking.component';

describe('EditarParkingComponent', () => {
  let component: EditarParkingComponent;
  let fixture: ComponentFixture<EditarParkingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarParkingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarParkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
