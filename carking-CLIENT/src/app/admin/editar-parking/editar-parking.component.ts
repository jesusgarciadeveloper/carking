import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IParkingmodificado } from 'src/app/interfaces/iparkingmodificado';
import { ParkingsService } from 'src/app/services/parkings.service';

@Component({
  selector: 'app-editar-parking',
  templateUrl: './editar-parking.component.html',
  styleUrls: ['./editar-parking.component.css']
})
export class EditarParkingComponent implements OnInit {

  parking: IParkingmodificado;

  constructor(
    private routerService: Router,
    private route: ActivatedRoute,
    private parkingsService: ParkingsService
  ) { }

  ngOnInit(): void {
    this.parking = {
      idparking: 0,
      nombre: '',
      direccion: '',
      numero: '',
      poblacion: '',
      provincia: '',
      codigopostal: '',
      tel: '',
      web: '',
      correo: '',
      preciomin: 0.00,
      preciomedh: 0.00,
      precioh: 0.00,
      preciodia: 0.00,
      preciomaxd: 0.00,
      apertura: '',
      cierre: '',
      estadocierre: '',
    };

    let idparking = this.route.snapshot.params.id;
    this.parkingsService.getParkingsId(idparking).subscribe(
      (res) => {
        res.forEach(element => {
          element.apertura = new Date(element.apertura.date);
          element.cierre = new Date(element.cierre.date);
        });
        this.parking = res[0];

      });
  }

  registro(): void {
    this.parkingsService.modificarParking(this.parking).subscribe(
      (res) => {
        alert("Parking modificado exitósamente");
        window.location.reload();
      }, (error) => alert(error.error.message)

    );
  }

  cambioFecha($event) {
    let d = new Date();
    d.setHours($event.target.value.split(':')[0], $event.target.value.split(':')[1], 0);

    if ($event.target.id == "aperturaModel") {
      this.parking.apertura = d.toUTCString();
    } else {
      this.parking.cierre = d.toUTCString();
    }
  }
}
