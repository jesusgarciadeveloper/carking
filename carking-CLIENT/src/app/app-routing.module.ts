import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AddArticuloComponent } from './admin/add-articulo/add-articulo.component';
import { AddFaqComponent } from './admin/add-faq/add-faq.component';
import { AddParkingComponent } from './admin/add-parking/add-parking.component';
import { AddPlazaComponent } from './admin/add-plaza/add-plaza.component';
import { AddUsuarioComponent } from './admin/add-usuario/add-usuario.component';
import { AdminArticulosComponent } from './admin/admin-articulos/admin-articulos.component';
import { AdminFaqsComponent } from './admin/admin-faqs/admin-faqs.component';
import { AdminParkingsComponent } from './admin/admin-parkings/admin-parkings.component';
import { AdminPlazasComponent } from './admin/admin-plazas/admin-plazas.component';
import { AdminUsuariosComponent } from './admin/admin-usuarios/admin-usuarios.component';
import { AdminComponent } from './admin/admin/admin.component';
import { EditarArticuloComponent } from './admin/editar-articulo/editar-articulo.component';
import { EditarFaqComponent } from './admin/editar-faq/editar-faq.component';
import { EditarParkingComponent } from './admin/editar-parking/editar-parking.component';
import { EditarPlazaComponent } from './admin/editar-plaza/editar-plaza.component';
import { EditarUsuarioComponent } from './admin/editar-usuario/editar-usuario.component';
import { AppComponent } from './app.component';
import { AddVehiculoPageComponent } from './components/add-vehiculo-page/add-vehiculo-page.component';
import { AvisoLegalComponent } from './components/aviso-legal/aviso-legal.component';
import { AyudaPageComponent } from './components/ayuda-page/ayuda-page.component';
import { ContactoPageComponent } from './components/contacto-page/contacto-page.component';
import { CookiesComponent } from './components/cookies/cookies.component';
import { DetalleArticuloPageComponent } from './components/detalle-articulo-page/detalle-articulo-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { MapaPageComponent } from './components/mapa-page/mapa-page.component';
import { ModificarusuarioPageComponent } from './components/modificarusuario-page/modificarusuario-page.component';
import { PrivacidadComponent } from './components/privacidad/privacidad.component';
import { RegistroPageComponent } from './components/registro-page/registro-page.component';
import { VistaPublicaComponent } from './components/vista-publica/vista-publica.component';
import { BackofficeGuard } from './guards/backoffice.guard';

const routes: Routes = [

  {
    path: 'admin',
    canActivate: [BackofficeGuard],
    component: AdminComponent,
    children: [
      { path: 'usuarios', component: AdminUsuariosComponent },
      { path: 'parkings', component: AdminParkingsComponent },
      { path: 'plazas', component: AdminPlazasComponent },
      { path: 'faqs', component: AdminFaqsComponent },
      { path: 'articulos', component: AdminArticulosComponent },
      { path: 'addusuario', component: AddUsuarioComponent },
      { path: 'addparking', component: AddParkingComponent },
      { path: 'addplaza', component: AddPlazaComponent },
      { path: 'addarticulo', component: AddArticuloComponent },
      { path: 'addfaq', component: AddFaqComponent },
      { path: 'editarusuario/:id', component: EditarUsuarioComponent },
      { path: 'editarparking/:id', component: EditarParkingComponent },
      { path: 'editarplaza/:id', component: EditarPlazaComponent },
      { path: 'editararticulo/:id', component: EditarArticuloComponent },
      { path: 'editarfaq/:id', component: EditarFaqComponent },
      { path: '**', redirectTo: '/admin/usuarios' }
    ]
  },
  {
    path: '',
    component: VistaPublicaComponent,
    children: [
      { path: 'login', component: LoginPageComponent },
      { path: 'ayuda', component: AyudaPageComponent },
      { path: 'contacto', component: ContactoPageComponent },
      { path: 'aviso', component: AvisoLegalComponent },
      { path: 'privacidad', component: PrivacidadComponent },
      { path: 'cookies', component: CookiesComponent },
      { path: 'registro', component: RegistroPageComponent },
      { path: 'modificausuario', component: ModificarusuarioPageComponent },
      { path: 'vehiculos', component: AddVehiculoPageComponent },
      { path: 'mapa', component: MapaPageComponent },
      { path: 'detallearticulo/:id', component: DetalleArticuloPageComponent },
      { path: '', component: HomePageComponent, pathMatch: "full" },
    ]
  },
  {
    path: '**',
    redirectTo: ''
  }
]
@NgModule({
  imports: [RouterModule.forRoot(routes, { anchorScrolling: 'enabled' }), BrowserModule],
  exports: [RouterModule],
})
export class AppRoutingModule { }
