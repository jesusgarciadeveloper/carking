import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Iarticulo } from 'src/app/interfaces/iarticulo';
import { ArticulosService } from 'src/app/services/articulos.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  fecha: Date;
  articulos: Array<Iarticulo>;

  constructor(
    private articulosService: ArticulosService,
    private routerService: Router
  ) {
    this.fecha = new Date();
    this.articulos = [];
  }

  ngOnInit(): void {
    this.articulosService.getLastArticulos().subscribe(
      (res) => {
        res.forEach(element => {

          this.articulos.push(element);

        });
      }, (error) => alert(error.error.message)

    );
  }
}
