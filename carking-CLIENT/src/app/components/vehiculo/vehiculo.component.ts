import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IVehiculo } from 'src/app/interfaces/ivehiculo';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {
  @Input() vehiculo: IVehiculo;
  @Input() i: number;
  @Output() eliminame = new EventEmitter();
  fechaAnyo: string;

  constructor() { }

  ngOnInit(): void {
    this.fechaAnyo = this.vehiculo.anyo;
  }

  eliminarVehiculo(vehiculo: IVehiculo) {
    this.eliminame.emit(vehiculo);
  }


  cambioFecha($event) {
    this.fechaAnyo = new Date($event.target.value).toUTCString();
    this.vehiculo.anyo = this.fechaAnyo;

  }
}
