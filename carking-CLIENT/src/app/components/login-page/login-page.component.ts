import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IUsuario } from 'src/app/interfaces/iusuario';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})
export class LoginPageComponent implements OnInit {
  constructor(
    private usuariosService: UsuariosService,
    private routerService: Router
  ) { }

  loginForm: FormGroup;
  usuario: IUsuario;
  email: string;
  pass: string;

  ngOnInit(): void {
    this.email = '';
    this.pass = '';

    if (this.usuariosService.isLogged() && this.usuariosService.isAdmin()) {
      this.routerService.navigate(['/admin']);
    }
    if (this.usuariosService.isLogged() && !this.usuariosService.isAdmin()) {
      this.routerService.navigate(['/mapa']);
    }

  }

  login(): void {
    this.usuariosService.login(this.email, this.pass).subscribe(
      (result) => {
        result.fechanac = new Date(result.fechanac);
        this.usuario = result;
        sessionStorage.setItem('usuario', JSON.stringify(result));
        if (this.usuario.isadmin) {
          this.routerService.navigate(['/admin']);
        } else {
          this.routerService.navigate(['']);
        }

      }, (error) => alert(error.message)

    );
  }
}
