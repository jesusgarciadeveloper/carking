import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVehiculoPageComponent } from './add-vehiculo-page.component';

describe('AddVehiculoPageComponent', () => {
  let component: AddVehiculoPageComponent;
  let fixture: ComponentFixture<AddVehiculoPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddVehiculoPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVehiculoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
