import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { INuevoUsuario } from 'src/app/interfaces/inuevo-usuario';
import { INuevoVehiculo } from 'src/app/interfaces/inuevo-vehiculo';
import { IUsuario } from 'src/app/interfaces/iusuario';
import { IVehiculo } from 'src/app/interfaces/ivehiculo';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { VehiculosService } from 'src/app/services/vehiculos.service';

@Component({
  selector: 'app-add-vehiculo-page',
  templateUrl: './add-vehiculo-page.component.html',
  styleUrls: ['./add-vehiculo-page.component.css']
})
export class AddVehiculoPageComponent implements OnInit {
  usuario: IUsuario;
  vehiculos: Array<IVehiculo>;

  constructor(
    private usuariosService: UsuariosService,
    private vehiculosService: VehiculosService,
    private routerService: Router
  ) {

  }

  ngOnInit(): void {
    this.usuario = JSON.parse(sessionStorage.getItem("usuario"));
    this.recargarVehiculos();
  }

  registro(): void {

    this.vehiculos.forEach(element => {

      if (element.idvehiculo == 0) {

        this.vehiculosService.insertVehiculo(element, this.usuario.idusuario).subscribe(
          (res) => {

            res.anyo = new Date(res.anyo.date);
            
          }, (error) => console.log(error)

        );
      } else {

        this.vehiculosService.modificaVehiculo(element).subscribe(

          (res) => {
            res.anyo = new Date(res.anyo.date);
          }, (error) => console.log(error)

        );
      }
    });
    
    setTimeout(()=> window.location.reload(),500);
    alert("Cambios realizados correctamente");
  }

  recargarVehiculos() {
    this.vehiculosService.getCarsFromUser(this.usuario.idusuario).subscribe(
      (res) => {

        res.forEach(element => {
          element.anyo = new Date(element.anyo.date);
        });
        this.vehiculos = res;
      }, (error) => console.log(error)

    );
  }

  eliminarVehiculo(vehiculo: any) {

    this.vehiculosService.eliminarVehiculo(vehiculo.idvehiculo, this.usuario.idusuario).subscribe(
      (res) => {
        alert("Vehículo eliminado correctamente.")
        window.location.reload();

      }, (error) => console.log(error)

    );

  }

  addcar() {
    this.vehiculos.push({
      'idvehiculo': 0,
      'matricula': '',
      'marca': '',
      'modelo': '',
      'carroceria': '',
      'anyo': ''
    });
  }
}
