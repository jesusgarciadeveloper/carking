import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarusuarioPageComponent } from './modificarusuario-page.component';

describe('ModificarusuarioPageComponent', () => {
  let component: ModificarusuarioPageComponent;
  let fixture: ComponentFixture<ModificarusuarioPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificarusuarioPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarusuarioPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
