import { Component, OnInit } from '@angular/core';
import { IUsuario } from 'src/app/interfaces/iusuario';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { IUsuarioModificado } from 'src/app/interfaces/iusuariomodificado';
import { VehiculosService } from 'src/app/services/vehiculos.service';

@Component({
  selector: 'app-modificarusuario-page',
  templateUrl: './modificarusuario-page.component.html',
  styleUrls: ['./modificarusuario-page.component.css'],
})
export class ModificarusuarioPageComponent implements OnInit {
  usuario: IUsuario;
  pass: string;
  fechaNac: string;

  constructor(
    private usuariosService: UsuariosService,
    private vehiculosService: VehiculosService,
    private routerService: Router
  ) { }

  ngOnInit(): void {
    this.usuario = JSON.parse(this.usuariosService.getSesion());
    this.pass = '';
    this.fechaNac = this.usuario.fechanac;
  }

  submit(): void {
    let usuarioModificado: IUsuarioModificado = {
      idusuario: this.usuario.idusuario,
      nombre: this.usuario.nombre,
      apellidos: this.usuario.apellidos,
      fechanac: this.fechaNac,
      dni: this.usuario.dni,
      direccion: this.usuario.direccion,
      numero: this.usuario.numero,
      poblacion: this.usuario.poblacion,
      codigopostal: this.usuario.codigopostal,
      provincia: this.usuario.provincia,
      email: this.usuario.email,
      tel: this.usuario.tel,
      isadmin: this.usuario.isadmin,
      alias: this.usuario.alias,
      pass: this.pass
    };

    this.usuariosService.modificaUsuario(usuarioModificado).subscribe(
      (result) => {

        result.fechanac = new Date(result.fechanac);
        this.usuario = result;
        sessionStorage.setItem('usuario', JSON.stringify(this.usuario));
        alert("Datos modificados con éxito");
        window.location.reload();
      }, (error) => alert(error.error.message)

    );
  }

  cambiopass($event) {
    this.pass = $event.target.value;
  }

  cambioFecha($event) {
    this.fechaNac = new Date($event.target.value).toUTCString();
  }

  eliminar() {
    this.usuariosService.eliminarUsuario(this.usuario.idusuario).subscribe(
      (result) => {
        this.usuariosService.logout();
        this.routerService.navigate(['']);
      }, (error) => alert(error.error.message)

    );
  }
}
