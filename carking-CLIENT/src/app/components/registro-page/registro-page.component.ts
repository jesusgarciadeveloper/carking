import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { INuevoUsuario } from 'src/app/interfaces/inuevo-usuario';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-registro-page',
  templateUrl: './registro-page.component.html',
  styleUrls: ['./registro-page.component.css'],
})
export class RegistroPageComponent implements OnInit {
  usuario: INuevoUsuario;
  repass: string;
  constructor(
    private usuariosService: UsuariosService,
    private routerService: Router
  ) { }

  ngOnInit(): void {
    this.usuario = {
      tel: '',
      nombre: '',
      apellidos: '',
      alias: '',
      dni: '',
      fechanac: '',
      direccion: '',
      numero: '',
      codigopostal: '',
      poblacion: '',
      provincia: '',
      email: '',
      pass: '',
      isadmin: false,
    };
    this.repass = '';
  }

  registro(): void {
    if (this.comprobarPass()) {
      this.usuariosService.nuevoUsuario(this.usuario).subscribe(
        (result) => {
          alert("Registro realizado correctamente.")
          result.fechanac = new Date(result.fechanac);
          this.usuario = result;
          sessionStorage.setItem('usuario', JSON.stringify(result));
          this.routerService.navigate(['']);
        }, (error) => alert(error.error.message)

      );
    }
  }

  cambioFecha($event) {
    this.usuario.fechanac = new Date($event.target.value).toUTCString();

  }

  comprobarPass(): boolean {
    if (this.usuario.pass !== this.repass) {
      alert('Las contraseñas no coinciden. Por favor, revíselas.');
      return false;
    }
    return true;
  }
}
