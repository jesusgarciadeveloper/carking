import { Component, Input, OnInit } from '@angular/core';
import { Iarticulo } from 'src/app/interfaces/iarticulo';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {
  @Input() articulo: Iarticulo;
  img: any;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.img = this.makeTrustedImage();

  }

  makeTrustedImage() {

    return this.sanitizer.bypassSecurityTrustUrl(this.articulo.imgsrc);
  }
}
