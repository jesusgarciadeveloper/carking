import { Component, OnInit } from '@angular/core';
import { FaqsService } from 'src/app/services/faqs.service';
import { Router } from '@angular/router';
import { FaqComponent } from '../faq/faq.component';

@Component({
  selector: 'app-ayuda-page',
  templateUrl: './ayuda-page.component.html',
  styleUrls: ['./ayuda-page.component.css']
})
export class AyudaPageComponent implements OnInit {
  faqs: Array<FaqComponent>;

  constructor(
    private faqsService: FaqsService,
    private routerService: Router
  ) {
    this.faqs = [];

  }

  ngOnInit(): void {
    this.faqsService.getAllFaqs().subscribe(
      (res) => {
        res.forEach(element => {
          this.faqs.push(element);
        });
      }, (error) => alert(error.error.message)

    );
  }

}
