import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IParking } from 'src/app/interfaces/iparking';
import { IPlaza } from 'src/app/interfaces/iplaza';
import { ParkingsService } from 'src/app/services/parkings.service';
import { PlazasService } from 'src/app/services/plazas.service';
import { timer } from 'rxjs';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { IUsuario } from 'src/app/interfaces/iusuario';

@Component({
  selector: 'app-mapa-page',
  templateUrl: './mapa-page.component.html',
  styleUrls: ['./mapa-page.component.css'],
})
export class MapaPageComponent implements OnInit {
  lat;
  lng;
  plazas;
  usuarioSesion: IUsuario;
  reservasUsuario;
  reservas;
  zoom = 16;
  origin: any;
  destination: any;
  visible: boolean;
  reservando: boolean;
  indicar: boolean = false;
  parking: IParking = null;

  plazaEscogida: IPlaza = null;
  plazaReservada: IPlaza = null;

  _second = 1000;
  _minute = this._second * 60;
  _hour = this._minute * 60;
  _day = this._hour * 24;
  end: any;
  now: any;
  day: any;
  hours: any;
  minutes: any;
  seconds: any;
  source = timer(0, 1000);
  clock: any;

  codigoReserva: string;

  iconoLibre = "/assets/logo_48x48px.png";
  iconoReservada = "/assets/logo_48x48px_reservada.png";
  iconoElectrico = "/assets/logo_48x48px_electricos.png";
  iconoMinusvalido = "/assets/logo_48x48px_minusvalidos.png";

  public mapStyles = [
    {
      "featureType": "poi",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ];

  options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };


  constructor(
    private plazasService: PlazasService,
    private usuariosService: UsuariosService,
    private parkingsService: ParkingsService,
    private routerService: Router
  ) {
    this.lat = 38.5421349;
    this.lng = -0.134407;
    this.plazas = [];
    this.reservasUsuario = [];

    this.visible = false;
    this.reservando = false;
  }


  ngOnInit(): void {
    this.recuperarPlazas();
    this.recuperarReservas();
    this.getCurrentLocation();
    this.usuarioSesion = JSON.parse(this.usuariosService.getSesion());

    if (sessionStorage.getItem("plazaReservada")) {


      this.plazasService.getReservas(JSON.parse(sessionStorage.getItem("usuario")).idusuario).subscribe(
        (res) => {
          res.forEach(element => {
            element.diahora = new Date(element.diahora.date);
            this.plazaReservada = this.plazas.find(obj => obj.idplaza == element.idplaza);
            //this.plazaReservada = JSON.parse(sessionStorage.getItem("plazaReservada"));
            this.plazaEscogida = JSON.parse(sessionStorage.getItem("plazaEscogida"));
            this.reservasUsuario.push(element);
          });

          this.parkingsService.getParking(this.plazaEscogida.idparking).subscribe(
            (res) => {
              res.apertura = new Date(res.apertura.date);
              res.cierre = new Date(res.cierre.date);
              this.parking = res;
            }, (error) => console.log(error)

          );
          this.visible = false;
          this.reservando = true;
          this.comenzarContador(true);
        }, (error) => console.log(error)

      );



    }

  }

  recuperarReservas() {
    this.reservas = [];
    this.plazasService.getAllReservas().subscribe(
      (res) => {
        res.forEach(element => {
          element.diahora = new Date(element.diahora.date);
          this.reservas.push(element);
        });
        sessionStorage.setItem("reservas", JSON.stringify(this.reservas));
      }, (error) => console.log(error)

    );
  }

  cerrar($event) {
    this.visible = false;
  }

  cerrarReservando($event) {
    this.reservando = false;
  }

  recuperarPlazas() {
    this.plazas = [];
    this.plazasService.getAllPlazas().subscribe(
      (res) => {

        res.forEach(element => {
          element.longitud = parseFloat(element.longitud);
          element.latitud = parseFloat(element.latitud);

          this.plazas.push(element);
        });
      }, (error) => alert(error.error.message)

    )
  }

  tarjeta(plaza: IPlaza) {

    var reservasSession = JSON.parse(sessionStorage.getItem("reservas"));

    var result = reservasSession?.find(obj => obj.idusuario === this.usuarioSesion.idusuario);
    this.plazaEscogida = plaza;
    sessionStorage.setItem('plazaEscogida', JSON.stringify(this.plazaEscogida));

    if (result == undefined) {

      this.parkingsService.getParking(plaza.idparking).subscribe(
        (res) => {
          res.apertura = new Date(res.apertura.date);
          res.cierre = new Date(res.cierre.date);
          this.parking = res;
        }, (error) => console.log(error)

      );
      this.visible = true;
    } else {
      this.visible = false;
      this.reservando = true;
      var result = reservasSession?.find(obj => obj.idplaza == this.plazaEscogida.idplaza);
      this.plazaReservada = JSON.parse(sessionStorage.getItem('plazaReservada'));
      if (result == undefined) {
        alert("Tienes una reserva activa");
      }

      this.lat = this.plazaReservada.latitud;
      this.lng = this.plazaReservada.longitud;
    }
  }

  generaCodigoReserva() {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = 20;

    for (let i = 0; i < charactersLength; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }

  reservaplaza() {
    this.recuperarReservas();
    this.recuperarPlazas();
    this.visible = false;
    this.plazaEscogida = JSON.parse(sessionStorage.getItem('plazaEscogida'));
    var result = [];
    var reservasSession = JSON.parse(sessionStorage.getItem("reservas"));

    if (reservasSession) {

      for (var i = 0, len = reservasSession.length; i < len; i++) {
        if (reservasSession[i].idplaza == this.plazaEscogida.idplaza) {
          result.push(reservasSession[i]);
          break;
        }

      }
    }
    
    if (result.length > 0) {

      alert("Plaza ya reservada por otro usuario");
      this.reservando = false;
      this.visible = false;
    } else {
      var result2 = reservasSession?.find(obj => obj.idusuario === this.usuarioSesion.idusuario);

      if (result2 == undefined) {

        this.codigoReserva = this.generaCodigoReserva();
        this.plazasService.reservarPlaza(this.plazaEscogida.idplaza, JSON.parse(sessionStorage.getItem("usuario")).idusuario, this.codigoReserva).subscribe(

          (res) => {
            let reserva = {
              'idusuario': res.idusuario,
              'idplaza': res.idplaza,
              'codigoreserva': res.codigoreserva,
              'diahora': new Date(res.diahora.date)
            };

            this.comenzarContador();
            sessionStorage.setItem("plazaReservada", JSON.stringify(this.plazaEscogida));
            this.reservas.push(reserva);
            sessionStorage.setItem("reservas", JSON.stringify(this.reservas));
            window.location.reload();

          }, (error) => console.log(error)

        );
        this.reservando = true;
        this.visible = false;
      } else {

        this.comenzarContador(true);
        //alert("Tienes una reserva activa");
        this.reservando = true;
        this.visible = false;

      }
    }

  }

  comenzarContador(existeContador: boolean = false) {

    this.clock = this.source.subscribe(t => {
      this.now = new Date();


      if (!existeContador) {
        let ahora = new Date();
        let segundos = ahora.getSeconds();
        let minutos = ahora.getMinutes() + 5;
        let hora = ahora.getHours();
        this.end = new Date('01/01/' + (this.now.getFullYear() + 1) + " " + hora + ":" + minutos + ":" + segundos);
      } else {
        var reservasSession = JSON.parse(sessionStorage.getItem("reservas"));
        var reservaUsuario = reservasSession?.find(obj => obj.idusuario == this.usuarioSesion.idusuario);

        if (reservaUsuario != undefined) {
          let antes = new Date(reservaUsuario.diahora);
          let segundos = antes.getSeconds();
          let minutos = antes.getMinutes() + 5;
          let hora = antes.getHours();
          this.end = new Date('01/01/' + (this.now.getFullYear() + 1) + " " + hora + ":" + minutos + ":" + segundos);
        }
      }

      this.showDate();
    });

  }

  showDate() {


    let distance = this.end - this.now;

    this.day = Math.floor(distance / this._day);
    this.hours = Math.floor((distance % this._day) / this._hour);
    this.minutes = Math.floor((distance % this._hour) / this._minute);
    this.seconds = Math.floor((distance % this._minute) / this._second);
    sessionStorage.setItem("end", this.end);

    if (this.minutes == 0 && this.seconds == 0) {
      this.anularReserva(0);
    }
  }

  anularReserva(estado) {

    var reservasSession = JSON.parse(sessionStorage.getItem("reservas"));

    var result = reservasSession?.find(obj => obj.idusuario === this.usuarioSesion.idusuario);

    this.plazasService.cancelarReserva(JSON.parse(sessionStorage.getItem("plazaReservada")).idplaza, result.codigoreserva, estado).subscribe(
      (res) => {
        let reserva = {
          'idusuario': this.usuarioSesion.idusuario,
          'idplaza': JSON.parse(sessionStorage.getItem("plazaReservada")).idplaza,
          'codigoreserva': result.codigoreserva,
          'diahora': new Date()
        };
        this.plazaEscogida = null;
        this.plazaReservada = null;
        sessionStorage.removeItem("end");
        sessionStorage.removeItem("plazaReservada");

        this.recuperarReservas();
        this.reservando = false;
        this.indicar = false;

        this.clock.unsubscribe();
        window.location.reload();
      }, (error) => console.log(error)

    );
  }

  llegada() {

  }

  irAPlaza() {
    this.origin = { lat: this.lat, lng: this.lng };
    this.destination = { lat: this.plazaEscogida.latitud, lng: this.plazaEscogida.longitud };
    this.indicar = true;
  }

  getDestination($event) {
    this.visible = !this.visible;

    let plaza = $event;
    this.tarjeta(plaza);
  }

  getCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {

          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;

        },
        (error) => this.showError(error),
        this.options);
    }
    else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  showError(error) {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        alert("User denied the request for Geolocation.");
        break;
      case error.POSITION_UNAVAILABLE:
        alert("Location information is unavailable.");
        break;
      case error.TIMEOUT:
        alert("The request to get user location timed out.");
        break;
      case error.UNKNOWN_ERROR:
        alert("An unknown error occurred.");
        break;
    }
  }
}
