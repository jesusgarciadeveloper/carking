import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor(
    private usuariosService: UsuariosService,
    public routerService: Router
  ) { }

  ngOnInit(): void {
  }

  isLogged(): boolean {
    return this.usuariosService.isLogged();
  }

  isAdmin(): boolean {
    return this.usuariosService.isAdmin();
  }

  logout(): void {
    this.usuariosService.logout();
    alert("La sesión ha sido cerrada correctamente.");
    this.routerService.navigate(['']);
  }
}
