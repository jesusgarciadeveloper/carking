import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleArticuloPageComponent } from './detalle-articulo-page.component';

describe('DetalleArticuloPageComponent', () => {
  let component: DetalleArticuloPageComponent;
  let fixture: ComponentFixture<DetalleArticuloPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleArticuloPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleArticuloPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
