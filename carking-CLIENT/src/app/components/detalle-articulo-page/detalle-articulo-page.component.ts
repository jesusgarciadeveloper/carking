import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Iarticulo } from 'src/app/interfaces/iarticulo';
import { ArticulosService } from 'src/app/services/articulos.service';

@Component({
  selector: 'app-detalle-articulo-page',
  templateUrl: './detalle-articulo-page.component.html',
  styleUrls: ['./detalle-articulo-page.component.css']
})
export class DetalleArticuloPageComponent implements OnInit {
  articulo: Iarticulo;

  constructor(
    private articulosService: ArticulosService,
    private routerService: Router,
    private route: ActivatedRoute
  ) {
    this.articulo = {
      idarticulo: 0,
      idautor: 1,
      titulo: "",
      contenido: "",
      imgsrc: "",
      fechapub: ""
    }
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.articulosService.getArticulo(id).subscribe(
      (res) => {
        res.fechapub = new Date(res.fechapub.date);
        this.articulo = res;
      }, (error) => alert(error.error.message)

    );
  }

}
