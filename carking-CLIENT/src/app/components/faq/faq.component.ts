import { Component, Input, OnInit } from '@angular/core';
import { Ifaq } from 'src/app/interfaces/ifaq';


@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
  @Input() faq: Ifaq;

  constructor(

  ) {
  }

  ngOnInit(): void {

  }

}
