import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IEmail } from 'src/app/interfaces/iemail';
import { EmailService } from 'src/app/services/email.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-contacto-page',
  templateUrl: './contacto-page.component.html',
  styleUrls: ['./contacto-page.component.css']
})
export class ContactoPageComponent implements OnInit {

  email: IEmail;



  constructor(
    private emailService: EmailService,
    private usuariosService: UsuariosService,
    private routerService: Router
  ) {
  }

  ngOnInit(): void {
    this.email = {
      nombre: '',
      tel: '',
      email: '',
      asunto: '',
      mensaje: ''
    }

    if (this.usuariosService.isLogged()) {
      let usuarioLogeado = JSON.parse(this.usuariosService.getSesion());
      this.email.nombre = usuarioLogeado.nombre;
      this.email.tel = usuarioLogeado.tel;
      this.email.email = usuarioLogeado.email;
    }
  }

  envio(): void {
    this.emailService.sendEmail(this.email).subscribe(
      (res) => {
        alert(res.mensaje);
        window.location.reload()
      }, (error) => alert(error.error.message)

    );
  }
}
