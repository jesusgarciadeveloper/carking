export interface INuevaPlaza {
  'idparking': number,
  'codigo': string,
  'planta': number,
  'tipo': string,
  'longitud': string
  'latitud': string
}
