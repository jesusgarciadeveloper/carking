export interface INuevoUsuario {
  nombre: string;
  apellidos: string;
  fechanac: string;
  dni: string;
  direccion: string;
  numero: string;
  codigopostal: string;
  poblacion: string;
  provincia: string;
  tel: string;
  email: string;
  alias: string;
  pass: string;
  isadmin: boolean;
}
