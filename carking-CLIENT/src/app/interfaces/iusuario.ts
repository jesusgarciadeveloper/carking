import { Timestamp } from 'rxjs';

export interface IUsuario {
  idusuario: number;
  dni: string;
  alias: string;
  nombre: string;
  apellidos: string;
  fechanac: string;
  direccion: string;
  numero: string;
  poblacion: string;
  provincia: string;
  codigopostal: string;
  tel: string;
  email: string;
  isadmin: boolean;
}
