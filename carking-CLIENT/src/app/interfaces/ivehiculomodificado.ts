export interface IVehiculomodificado {
  'idvehiculo': number,
  'matricula': string,
  'marca': string,
  'modelo': string,
  'carroceria': string,
  'anyo': string
}
