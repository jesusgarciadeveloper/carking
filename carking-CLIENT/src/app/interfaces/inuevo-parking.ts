export interface INuevoParking {
  'nombre': string,
  'direccion': string,
  'numero': string,
  'poblacion': string,
  'provincia': string,
  'codigopostal': string,
  'tel': string,
  'web': string,
  'correo': string,
  'preciomin': number,
  'preciomedh': number,
  'precioh': number,
  'preciodia': number,
  'preciomaxd': number,
  'apertura': string,
  'cierre': string,
  'estadocierre': string
}
