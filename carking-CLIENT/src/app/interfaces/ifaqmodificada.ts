export interface Ifaqmodificada {
  idfaq: number,
  idautor: number,
  pregunta: string,
  respuesta: string
}
