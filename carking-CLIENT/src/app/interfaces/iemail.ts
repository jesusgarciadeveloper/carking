export interface IEmail {
  nombre: string,
  tel: string,
  email: string,
  asunto: string,
  mensaje: string
}
