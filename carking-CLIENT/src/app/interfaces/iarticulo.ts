export interface Iarticulo {

  idarticulo: number,
  idautor: number,
  titulo: string,
  contenido: string,
  imgsrc: string,
  fechapub: string
}

