export interface IPlaza {
  'idplaza': number,
  'idparking': number,
  'planta': number,
  'codigo': string,
  'estado': number,
  'tipo': string,
  'longitud': string
  'latitud': string
}
