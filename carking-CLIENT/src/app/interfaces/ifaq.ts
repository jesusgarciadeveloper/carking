export interface Ifaq {
  idfaq: number,
  idautor: number,
  pregunta: string,
  respuesta: string
}
