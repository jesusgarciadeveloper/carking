export interface InuevoArticulo {
  idautor: number,
  titulo: string,
  contenido: string,
  imgsrc: string
}
