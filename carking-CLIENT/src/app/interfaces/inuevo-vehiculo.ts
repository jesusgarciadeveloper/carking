export interface INuevoVehiculo {
  'matricula': string,
  'marca': string,
  'modelo': string,
  'carroceria': string,
  'anyo': string
}
