export interface IPlazamodificada {
  'idplaza': number,
  'idparking': number,
  'codigo': string,
  'planta': number,
  'estado': number,
  'tipo': string,
  'longitud': string,
  'latitud': string
}
