import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localePy from '@angular/common/locales/es-PY';
registerLocaleData(localePy, 'es');

import { UsuariosService } from './services/usuarios.service';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { AyudaPageComponent } from './components/ayuda-page/ayuda-page.component';
import { ContactoPageComponent } from './components/contacto-page/contacto-page.component';
import { RegistroPageComponent } from './components/registro-page/registro-page.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ModificarusuarioPageComponent } from './components/modificarusuario-page/modificarusuario-page.component';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';   // agm-direction
import { FaqComponent } from './components/faq/faq.component';
import { ArticuloComponent } from './components/articulo/articulo.component';
import { DetalleArticuloPageComponent } from './components/detalle-articulo-page/detalle-articulo-page.component';
import { AdminModule } from './admin/admin.module';
import { VistaPublicaComponent } from './components/vista-publica/vista-publica.component';
import { AddVehiculoPageComponent } from './components/add-vehiculo-page/add-vehiculo-page.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { MapaPageComponent } from './components/mapa-page/mapa-page.component';
import { FooterComponent } from './components/footer/footer.component';
import { AvisoLegalComponent } from './components/aviso-legal/aviso-legal.component';
import { PrivacidadComponent } from './components/privacidad/privacidad.component';
import { CookiesComponent } from './components/cookies/cookies.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    AyudaPageComponent,
    ContactoPageComponent,
    RegistroPageComponent,
    NavbarComponent,
    ModificarusuarioPageComponent,
    FaqComponent,
    ArticuloComponent,
    DetalleArticuloPageComponent,
    VistaPublicaComponent,
    AddVehiculoPageComponent,
    VehiculoComponent,
    MapaPageComponent,
    FooterComponent,
    AvisoLegalComponent,
    PrivacidadComponent,
    CookiesComponent],
  imports: [

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AdminModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCiek4btZjGlf1LJsHevGrT3vkWJeyf9Lo'
    }),
    AgmDirectionModule
  ],
  providers: [UsuariosService,
    { provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
