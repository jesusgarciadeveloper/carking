<?php

/**
 * ARRAY OF CONTROLLERS ASSIGNED MANUAL
 * IF METHOD IS NOT DEFINED OR EMPTY, IT TAKES THE DEFAULT METHOD
 * THE CONTROLLER ROUTES ARE RELATIVE TO THE CONTROLLERS DIRECTORY
 */
$routes = [
    [
        'uri'            => '/usuario/login',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'loginUser',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/usuario/logout',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'logoutUser',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/usuario/update',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'actualizarusuario',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/usuario/todosUsuarios',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'getAllUsers',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/usuario/',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'nuevoUsuario',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/usuario/',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'getUsuario',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/usuario/alias',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'getUsuarioAlias',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/usuario/',
        'controllerPath' => 'UsuarioRESTController',
        'method'         => 'eliminarUsuario',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/faq/',
        'controllerPath' => 'FaqRESTController',
        'method'         => 'getAllFaqs',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/faq/',
        'controllerPath' => 'FaqRESTController',
        'method'         => 'insertarFaq',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/faq/',
        'controllerPath' => 'FaqRESTController',
        'method'         => 'deleteFaq',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/faq/id',
        'controllerPath' => 'FaqRESTController',
        'method'         => 'getFaqsId',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/faq/update',
        'controllerPath' => 'FaqRESTController',
        'method'         => 'modificarFaq',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/articulo/',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'getAllArticulos',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/articulo/update',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'modificarArticulo',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/articulo/',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'insertarArticulo',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/articulo/',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'deleteArticulo',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/articulo/last',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'getLastArticulos',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/articulo/id',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'getArticulosId',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/articulo/detalle',
        'controllerPath' => 'ArticuloRESTController',
        'method'         => 'getArticulo',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/enviar',
        'controllerPath' => 'EmailController',
        'method'         => 'sendEmail',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/vehiculo/usuario',
        'controllerPath' => 'VehiculoRESTController',
        'method'         => 'getCarsFromUser',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/vehiculo/matricula',
        'controllerPath' => 'VehiculoRESTController',
        'method'         => 'existeMatricula',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/vehiculo/',
        'controllerPath' => 'VehiculoRESTController',
        'method'         => 'deleteCarFromUser',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/vehiculo/',
        'controllerPath' => 'VehiculoRESTController',
        'method'         => 'nuevoVehiculo',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/vehiculo/update',
        'controllerPath' => 'VehiculoRESTController',
        'method'         => 'actualizarVehiculo',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/plaza/',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'getAllPlazas',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/plaza/',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'insertarPlaza',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/plaza/',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'deletePlaza',
        'httpMethod'     => 'DELETE'
    ],
    [
        'uri'            => '/plaza/id',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'getPlazasId',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/plaza/idParking',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'getPlazasIdParking',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/plaza/cancelar',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'anularReserva',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/plaza/update',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'modificarPlaza',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/plaza/reservar',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'reservaPlaza',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/plaza/reservadeusuario',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'getReservasUsuario',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/plaza/todasreservas',
        'controllerPath' => 'PlazaRESTController',
        'method'         => 'getAllReservas',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/parking/getparking',
        'controllerPath' => 'ParkingRESTController',
        'method'         => 'getParking',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/parking/',
        'controllerPath' => 'ParkingRESTController',
        'method'         => 'getAllParkings',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/parking/',
        'controllerPath' => 'ParkingRESTController',
        'method'         => 'insertarParking',
        'httpMethod'     => 'POST'
    ],
    [
        'uri'            => '/parking/update',
        'controllerPath' => 'ParkingRESTController',
        'method'         => 'modificarParking',
        'httpMethod'     => 'PUT'
    ],
    [
        'uri'            => '/parking/id',
        'controllerPath' => 'ParkingRESTController',
        'method'         => 'getParkingsId',
        'httpMethod'     => 'GET'
    ],
    [
        'uri'            => '/parking/',
        'controllerPath' => 'ParkingRESTController',
        'method'         => 'deleteParking',
        'httpMethod'     => 'DELETE'
    ]


];
