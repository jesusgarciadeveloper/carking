<?php

/**
 * Articulo
 */
class Articulo
{
    /**
     * @var int
     */
    private $idarticulo;

    /**
     * @var string
     *
     */
    private $titulo;

    /**
     * @var string
     *
     */
    private $contenido;

    /**
     * @var string
     *
     */
    private $imgsrc;

    /**
     * @var \DateTime
     *
     */
    private $fechapub;

    /**
     * @var int
     * 
     */
    private $idautor;

    public function getIdarticulo(): int
    {
        return $this->idarticulo;
    }

    public function getTitulo(): string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getContenido(): string
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido): self
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getImgsrc(): string
    {
        return $this->imgsrc;
    }

    public function setImgsrc(string $imgsrc): self
    {
        $this->imgsrc = $imgsrc;

        return $this;
    }

    public function getFechapub(): \DateTimeInterface
    {
        return $this->fechapub;
    }

    public function setFechapub(\DateTimeInterface $fechapub): self
    {
        $this->fechapub = $fechapub;

        return $this;
    }

    public function getIdAutor(): int
    {
        return $this->idautor;
    }

    public function setIdAutor(int $idautor): self
    {
        $this->idautor = $idautor;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct(
        int $idarticulo,
        string $titulo,
        string $contenido,
        string $imgsrc = "",
        int $idautor,
        DateTimeImmutable $fechapub
    ) {
        $this->idarticulo = $idarticulo;
        $this->titulo = $titulo;
        $this->contenido = $contenido;
        $this->imgsrc = $imgsrc;
        $this->idautor = $idautor;
        $this->fechapub = $fechapub;
    }

    public function getPublicData(): array
    {
        return [
            'idarticulo' => $this->idarticulo,
            'titulo' => $this->titulo,
            'contenido' => $this->contenido,
            'imgsrc' => $this->imgsrc,
            'idautor' => $this->idautor,
            'fechapub' => $this->fechapub
        ];
    }
}
