<?php

/**
 * Plaza
 */
class Plaza
{
    /**
     * @var int
     */
    private $idplaza;

    /**
     * @var string
     */
    private $longitud;

    /**
     * @var string
     */
    private $latitud;

    /**
     * @var int
     */
    private $planta;

    /**
     * @var string|null
     */
    private $codigo;

    /**
     * @var int
     */
    private $estado;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @var int
     */
    private $idparking;

    public function getIdplaza(): int
    {
        return $this->idplaza;
    }

    public function getLongitud(): string
    {
        return $this->longitud;
    }

    public function setLongitud(string $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getLatitud(): string
    {
        return $this->latitud;
    }

    public function setLatitud(string $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getPlanta(): int
    {
        return $this->planta;
    }

    public function setPlanta(int $planta): self
    {
        $this->planta = $planta;

        return $this;
    }

    public function getCodigo(): string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getEstado(): int
    {
        return $this->estado;
    }

    public function setEstado(int $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getTipo(): string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getIdParking(): int
    {
        return $this->idparking;
    }

    public function setIdParking(int $idparking): self
    {
        $this->idparking = $idparking;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct(
        int $idplaza,
        int $idparking,
        int $planta = null,
        string $codigo,
        int $estado,
        string $tipo,
        string $longitud,
        string $latitud
    ) {
        $this->idplaza = $idplaza;
        $this->idparking = $idparking;
        $this->planta = $planta;
        $this->codigo = $codigo;
        $this->estado = $estado;
        $this->tipo = $tipo;
        $this->longitud = $longitud;
        $this->latitud = $latitud;
    }

    public function getPublicData(): array
    {
        return [
            "idplaza" => $this->idplaza,
            "idparking" => $this->idparking,
            "planta" => $this->planta,
            "codigo" => $this->codigo,
            "estado" => $this->estado,
            "tipo" => $this->tipo,
            "longitud" => $this->longitud,
            "latitud" => $this->latitud
        ];
    }
}
