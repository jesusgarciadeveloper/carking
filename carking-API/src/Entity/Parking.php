<?php

/**
 * Parking
 */
class Parking
{
    /**
     * @var int
     */
    private $idparking;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var string
     */
    private $numero;

    /**
     * @var string
     */
    private $poblacion;

    /**
     * @var string
     */
    private $provincia;

    /**
     * @var string
     */
    private $codigopostal;

    /**
     * @var string
     */
    private $tel;

    /**
     * @var string
     */
    private $web;

    /**
     * @var string
     *      
     */
    private $correo;

    /**
     * @var float
     */
    private $preciomin;

    /**
     * @var float
     */
    private $preciomedh;

    /**
     * @var float
     *
     * @ORM\Column(name="precioH", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $precioh;

    /**
     * @var float
     *
     */
    private $preciodia;

    /**
     * @var float
     *
     */
    private $preciomaxd;

    /**
     * @var DateTimeImmutable
     */
    private $apertura;

    /**
     * @var DateTimeImmutable
     */
    private $cierre;

    /**
     * @var string
     */
    private $estadocierre;

    public function getIdparking(): int
    {
        return $this->idparking;
    }

    public function getNombre(): string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDireccion(): string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getNumero(): string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getPoblacion(): string
    {
        return $this->poblacion;
    }

    public function setPoblacion(string $poblacion): self
    {
        $this->poblacion = $poblacion;

        return $this;
    }

    public function getProvincia(): string
    {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getCodigopostal(): string
    {
        return $this->codigopostal;
    }

    public function setCodigopostal(string $codigopostal): self
    {
        $this->codigopostal = $codigopostal;

        return $this;
    }

    public function getTel(): string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getWeb(): string
    {
        return $this->web;
    }

    public function setWeb(string $web): self
    {
        $this->web = $web;

        return $this;
    }

    public function getCorreo(): string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getPreciomin(): float
    {
        return $this->preciomin;
    }

    public function setPreciomin(float $preciomin): self
    {
        $this->preciomin = $preciomin;

        return $this;
    }

    public function getPreciomedh(): float
    {
        return $this->preciomedh;
    }

    public function setPreciomedh(float $preciomedh): self
    {
        $this->preciomedh = $preciomedh;

        return $this;
    }

    public function getPrecioh(): float
    {
        return $this->precioh;
    }

    public function setPrecioh(float $precioh): self
    {
        $this->precioh = $precioh;

        return $this;
    }

    public function getPreciodia(): float
    {
        return $this->preciodia;
    }

    public function setPreciodia(float $preciodia): self
    {
        $this->preciodia = $preciodia;

        return $this;
    }

    public function getPreciomaxd(): float
    {
        return $this->preciomaxd;
    }

    public function setPreciomaxd(float $preciomaxd): self
    {
        $this->preciomaxd = $preciomaxd;

        return $this;
    }

    public function getApertura(): DateTimeImmutable
    {
        return $this->apertura;
    }

    public function setApertura(DateTimeImmutable $apertura): self
    {
        $this->apertura = $apertura;

        return $this;
    }

    public function getCierre(): DateTimeImmutable
    {
        return $this->cierre;
    }

    public function setCierre(DateTimeImmutable $cierre): self
    {
        $this->cierre = $cierre;

        return $this;
    }

    public function getEstadocierre(): string
    {
        return $this->estadocierre;
    }

    public function setEstadocierre(string $estadocierre): self
    {
        $this->estadocierre = $estadocierre;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct(
        int $idparking,
        string $nombre,
        string $direccion,
        string $numero,
        string $poblacion,
        string $provincia,
        string $codigopostal,
        string $tel,
        string $web,
        string $correo,
        float $preciomin,
        float $preciomedh,
        float $precioh,
        float $preciodia,
        float $preciomaxd,
        DateTimeImmutable $apertura,
        DateTimeImmutable $cierre,
        string $estadocierre
    ) {
        $this->idparking = $idparking;
        $this->nombre = $nombre;
        $this->direccion = $direccion;
        $this->numero = $numero;
        $this->poblacion = $poblacion;
        $this->provincia = $provincia;
        $this->codigopostal = $codigopostal;
        $this->tel = $tel;
        $this->web = $web;
        $this->correo = $correo;
        $this->preciomin = $preciomin;
        $this->preciomedh = $preciomedh;
        $this->precioh = $precioh;
        $this->preciodia = $preciodia;
        $this->preciomaxd = $preciomaxd;
        $this->apertura = $apertura;
        $this->cierre = $cierre;
        $this->estadocierre = $estadocierre;
    }

    public function getPublicData(): array
    {
        return [
            "idparking" => $this->idparking,
            "nombre" => $this->nombre,
            "direccion" => $this->direccion,
            "numero" => $this->numero,
            "poblacion" => $this->poblacion,
            "provincia" => $this->provincia,
            "codigopostal" => $this->codigopostal,
            "tel" => $this->tel,
            "web" => $this->web,
            "correo" => $this->correo,
            "preciomin" => $this->preciomin,
            "preciomedh" => $this->preciomedh,
            "precioh" => $this->precioh,
            "preciodia" => $this->preciodia,
            "preciomaxd" => $this->preciomaxd,
            "apertura" => $this->apertura,
            "cierre" => $this->cierre,
            "estadocierre" => $this->estadocierre
        ];
    }
}
