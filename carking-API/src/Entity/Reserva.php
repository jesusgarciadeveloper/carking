<?php

/**
 * Reserva
 */
class Reserva
{
    /**
     * @var string
     *
     */
    private $codigoreserva;

    /**
     * @var \DateTimeImmutable
     */
    private $diahora;

    /**
     * @var int
     *
     */
    private $idplaza;

    /**
     * @var int
     *
     */
    private $idusuario;

    public function getCodigoreserva(): string
    {
        return $this->codigoreserva;
    }

    public function getDiahora(): \DateTimeInterface
    {
        return $this->diahora;
    }

    public function setDiahora(\DateTimeInterface $diahora): self
    {
        $this->diahora = $diahora;

        return $this;
    }

    public function getIdPlaza(): int
    {
        return $this->idplaza;
    }

    public function setIdPlaza(int $idplaza): self
    {
        $this->idplaza = $idplaza;

        return $this;
    }

    public function getIdUsuario(): int
    {
        return $this->idusuario;
    }

    public function setIdUsuario(int $idusuario): self
    {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct(
        string $codigoreserva,
        int $idusuario,
        int $idplaza,
        \DateTimeImmutable $diahora
    ) {
        $this->codigoreserva = $codigoreserva;
        $this->idusuario = $idusuario;
        $this->idplaza = $idplaza;
        $this->diahora = $diahora;
    }

    public function getPublicData(): array
    {
        return [
            'codigoreserva' => $this->codigoreserva,
            'idusuario' => $this->idusuario,
            'idplaza' => $this->idplaza,
            'diahora' => $this->diahora
        ];
    }
}
