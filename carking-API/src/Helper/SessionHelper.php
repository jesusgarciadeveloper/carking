<?php

class SessionHelper
{
    public static function initUserSession(Usuario $user): void
    {
        session_start();

        $_SESSION["usuario"] = $user;
        session_write_close();
    }

    public static function getUserSession(): Usuario
    {
        session_start();

        if (!isset($_SESSION['usuario'])) {
            throw new Exception('La sesión del usuario no existe.');
        }
        $user = $_SESSION['usuario'];
        session_write_close();
        return $user;
    }

    public static function closeUserSession(): void
    {
        session_start();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }
        session_destroy();
    }
}
