<?php

require_once(ENTITYPATH . "Usuario.php");
require_once(REPOSITORYPATH . "UsuarioRepository.php");
require_once(REPOSITORYPATH . "VehiculoRepository.php");
require_once(CTRLPATH . 'CoreController.php');
require_once(HELPERPATH . 'SessionHelper.php');

class UsuarioRESTController extends CoreController
{

    public function __construct()
    {
    }

    public function loginUser()
    {

        $userModel = new UsuarioRepository();

        $dataArray = json_decode(file_get_contents("php://input"), true);

        $user = $userModel->getLoginUser($dataArray["idWord"], hash("sha256", $dataArray["pass"]));

        if (!$user) {
            $this->sendErrorMessage(400, 400, "Usuario o contraseña incorrectos.");
        }


        header('Content-Type: application/json');
        die(json_encode($user->getPublicData()));
    }

    public function logoutUser()
    {
        session_start();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }
        session_destroy();
    }

    public function nuevoUsuario()
    {
        $request = json_decode(file_get_contents("php://input"), true);

        $usuario = new Usuario(
            0,
            $request['nombre'],
            $request['apellidos'],
            $request['dni'],
            $request['alias'],
            new DateTimeImmutable($request['fechanac']),
            $request['direccion'],
            $request['numero'],
            $request['poblacion'],
            $request['provincia'],
            $request['codigopostal'],
            $request['tel'],
            $request['email'],
            $request['isadmin'],
            hash("sha256", $request['pass'])
        );

        $userModel = new UsuarioRepository();
        $existAlias = count($userModel->checkIfAliasExist($usuario->getAlias()));
        $existEmail = count($userModel->checkIfEmailExist($usuario->getEmail()));

        if ($existAlias > 0 && $existEmail > 0) {
            $this->sendErrorMessage(200, 4001, "Usuario ya existe");
        }

        if ($existAlias > 0) {
            $this->sendErrorMessage(200, 4011, "Alias ya existe");
        }

        if ($existEmail > 0) {
            $this->sendErrorMessage(200, 4021, "Email ya existe");
        }


        try {
            $usuarioIngresado = $userModel->insertUser($usuario);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }


        header('Content-Type: application/json');
        die(json_encode($usuarioIngresado->getPublicData()));
    }

    public function eliminarUsuario()
    {



        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del usuario no existe");
        }

        $userId = $_GET['id'];


        $userModel = new UsuarioRepository();
        $numAdmin = $userModel->getNumAdmin();

        if ($numAdmin <= 1) {
            $this->sendErrorMessage(400, 4002, "No se puede eliminar este usuario porque es el único Administrador de la aplicación.");
        }


        $vehiculoModel = new VehiculoRepository();

        $vehiculos = $vehiculoModel->getCarsFromUser($userId);

        foreach ($vehiculos as $car) {

            $vehiculoModel->deleteTitularidadVehiculo($car->getIdvehiculo(), $userId);

            $numPropietarios = $vehiculoModel->getNumPropietarios($car->getIdvehiculo());

            if ($numPropietarios == 0) {
                try {
                    $affectedRows = $vehiculoModel->deleteVehiculo($car->getIdvehiculo());
                } catch (PDOException $e) {
                    $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
                }

                if ($affectedRows <= 0) {
                    $this->sendErrorMessage(200, 2001, "Vehículo no encontrado");
                }
            }
        }





        try {
            $affectedRows = $userModel->deleteUser($userId);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($affectedRows <= 0) {
            $this->sendErrorMessage(200, 2001, "Usuario no encontrado");
        }

        $this->sendErrorMessage(201, 2002, "Usuario eliminado correctamente");
    }

    public function getAllUsers()
    {


        $userModel = new UsuarioRepository();
        $arrUsers  = $userModel->getAllUsers();
        $result    = null;
        foreach ($arrUsers as $user) {
            $result[] = $user->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }


    public function actualizarusuario()
    {


        $userModel = new UsuarioRepository();
        $request = json_decode(file_get_contents("php://input"), true);

        try {


            $userBD = $userModel->getUserRow($request["idusuario"]);

            if (empty($userBD)) {
                $this->sendErrorMessage(400, 2001, "Usuario no encontrado");
            }

            $userBD = $userModel->getUser($userBD);

            $userBD->setIsadmin($request["isadmin"]);


            if ($request["alias"] != $userBD->getAlias()) {
                $existAlias = count($userModel->checkIfAliasExist($request["alias"]));
            } else {
                $existAlias = -1;
            }

            if ($request["email"] != $userBD->getEmail()) {
                $existEmail = count($userModel->checkIfEmailExist($request["email"]));
            } else {
                $existEmail = -1;
            }

            if ($existAlias > 0 && $existEmail > 0) {
                $this->sendErrorMessage(400, 4001, "Usuario ya existe");
            }

            if ($existAlias > 0) {
                $this->sendErrorMessage(400, 4011, "Alias ya existe");
            }

            if ($existEmail > 0) {
                $this->sendErrorMessage(400, 4021, "Email ya existe");
            }

            $userBD->setNombre($request['nombre']);
            $userBD->setApellidos($request['apellidos']);
            $userBD->setDni($request['dni']);
            $userBD->setAlias($request["alias"]);
            $userBD->setFechanac(new DateTimeImmutable($request["fechanac"]));
            $userBD->setDireccion($request["direccion"]);
            $userBD->setNumero($request["numero"]);
            $userBD->setPoblacion($request["poblacion"]);
            $userBD->setProvincia($request["provincia"]);
            $userBD->setCodigopostal($request["codigopostal"]);
            $userBD->setTel($request["tel"]);
            $userBD->setEmail($request["email"]);
            if (empty($request["pass"] == false)) {
                $userBD->setPass(hash("sha256", $request["pass"]));
            }

            $affectedRows = $userModel->updateUser($userBD);
        } catch (Exception $e) {
            $this->sendErrorMessage((int)$e->getCode(), (int) $e->getCode(), $e->getMessage());
        }


        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($userBD->getPublicData()));
    }


    public function getUsuario()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del usuario no existe");
        }

        $userId = intval($_GET['id']);

        $userModel = new UsuarioRepository();
        $usuario = $userModel->getUserRow($userId);

        $usuario = $userModel->getUser($usuario);
        header('Content-Type: application/json');
        die(json_encode($usuario->getPublicData()));
    }
    public function getUsuarioAlias()
    {

        if (!isset($_GET['alias'])) {
            $this->sendErrorMessage(400, 4004, "El alias del usuario no existe");
        }

        $userAlias = $_GET['alias'];


        $userModel = new UsuarioRepository();
        $usuarios = $userModel->getUserAliasRow($userAlias);
        $result    = [];
        foreach ($usuarios as $user) {
            $result[] = $user->getPublicData();
        }
        header('Content-Type: application/json');
        die(json_encode($result));
    }
}
