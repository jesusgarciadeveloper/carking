<?php
require_once(ENTITYPATH . "Parking.php");
require_once(REPOSITORYPATH . "ParkingRepository.php");
require_once(REPOSITORYPATH . "PlazaRepository.php");

require_once(CTRLPATH . 'CoreController.php');

class ParkingRESTController extends CoreController
{

    public function __construct()
    {
    }

    public function getAllParkings()
    {
        $plazaModel = new ParkingRepository();
        $arrParkings = $plazaModel->getAllParkings();
        $result    = [];
        foreach ($arrParkings as $parking) {
            $result[] = $parking->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getParkingsId()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del parking no existe");
        }

        $parkingId = $_GET['id'];

        $parkingModel = new ParkingRepository();
        $parkings = $parkingModel->getParkingsId($parkingId);
        $result    = [];
        foreach ($parkings as $parking) {
            $result[] = $parking->getPublicData();
        }
        header('Content-Type: application/json');
        die(json_encode($result));
    }

    public function getParking()
    {
        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del parking no existe");
        }

        $parkingId = $_GET['id'];

        $parkingModel = new ParkingRepository();
        $parking = $parkingModel->getParkingRow($parkingId);
        $parking = $parkingModel->getParking($parking);
        header('Content-Type: application/json');
        die(json_encode($parking->getPublicData()));
    }

    public function deleteParking()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del parking no existe");
        }

        $parkingId = $_GET['id'];

        $parkingModel = new ParkingRepository();
        
        
        try {
            $plazaModel = new PlazaRepository();
        $plazas = $plazaModel->getPlazasIdParking($parkingId);
        $result    = [];
        foreach ($plazas as $plaza) {
            $result[] = $plaza->getPublicData();
        }
        foreach ($result as $plaza) {
            $plazaModel->deletePlaza(strval($plaza["idplaza"]));
        }
            $affectedRows = $parkingModel->deleteParking($parkingId);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($affectedRows <= 0) {
            $this->sendErrorMessage(200, 2001, "Parking no encontrado");
        }

        $this->sendErrorMessage(201, 2002, "Parking eliminado correctamente");
    }

    public function insertarParking()
    {
        $request = json_decode(file_get_contents("php://input"), true);

        $parking = new Parking(
            0,
            $request['nombre'],
            $request['direccion'],
            $request['numero'],
            $request['poblacion'],
            $request['provincia'],
            $request['codigopostal'],
            $request['tel'],
            $request['web'],
            $request['correo'],
            $request['preciomin'],
            $request['preciomedh'],
            $request['precioh'],
            $request['preciodia'],
            $request['preciomaxd'],
            new DateTimeImmutable($request['apertura']),
            new DateTimeImmutable($request['cierre']),
            $request['estadocierre']
        );

        $parkingModel = new ParkingRepository();

        try {
            $parkingInsertado = $parkingModel->insertarParking($parking);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }


        header('Content-Type: application/json');
        die(json_encode($parkingInsertado->getPublicData()));
    }

    public function modificarParking()
    {
        $parkingModel = new ParkingRepository();
        $request = json_decode(file_get_contents("php://input"), true);


        try {
            $parkingBD = $parkingModel->getParkingRow($request["idparking"]);
            if (empty($parkingBD)) {
                $this->sendErrorMessage(400, 2001, "Parking no encontrado");
            }
            $parkingBD = $parkingModel->getParking($parkingBD);

            $parkingBD->setNombre($request["nombre"]);
            $parkingBD->setDireccion($request["direccion"]);
            $parkingBD->setNumero($request["numero"]);
            $parkingBD->setPoblacion($request["poblacion"]);
            $parkingBD->setProvincia($request["provincia"]);
            $parkingBD->setCodigopostal($request["codigopostal"]);
            $parkingBD->setTel($request["tel"]);
            $parkingBD->setWeb($request["web"]);
            $parkingBD->setCorreo($request["correo"]);
            $parkingBD->setPreciomin($request["preciomin"]);
            $parkingBD->setPreciomedh($request["preciomedh"]);
            $parkingBD->setPrecioh($request["precioh"]);
            $parkingBD->setPreciodia($request["preciodia"]);
            $parkingBD->setPreciomaxd($request["preciomaxd"]);
            $parkingBD->setApertura(new DateTimeImmutable($request['apertura']));
            $parkingBD->setCierre(new DateTimeImmutable($request['cierre']));
            $parkingBD->setEstadocierre($request["estadocierre"]);


            $affectedRows = $parkingModel->updateParking($parkingBD);
        } catch (Exception $e) {
            $this->sendErrorMessage((int)$e->getCode(), (int) $e->getCode(), $e->getMessage());
        }

        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($parkingBD->getPublicData()));
    }
}
