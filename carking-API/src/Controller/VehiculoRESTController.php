<?php
require_once(ENTITYPATH . "Vehiculo.php");
require_once(REPOSITORYPATH . "VehiculoRepository.php");
require_once(CTRLPATH . 'CoreController.php');

class VehiculoRESTController extends CoreController
{

    public function __construct()
    {
    }

    public function existeMatricula()
    {
        if (!isset($_GET['matricula'])) {
            $this->sendErrorMessage(400, 4004, "La matrícula del vehiculo no existe");
        }

        $matricula = $_GET['matricula'];
        $vehiculoModel = new VehiculoRepository();
        $vehiculoBD = $vehiculoModel->existeMatricula($matricula);

        header('Content-Type: application/json');
        echo json_encode($vehiculoBD);
    }

    public function getCarsFromUser()
    {
        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del usuario no existe");
        }

        $usuarioId = $_GET['id'];

        $vehiculoModel = new VehiculoRepository();
        $vehiculos = $vehiculoModel->getCarsFromUser($usuarioId);
        $result    = [];
        foreach ($vehiculos as $vehiculo) {
            $result[] = $vehiculo->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function nuevoVehiculo()
    {
        $request = json_decode(file_get_contents("php://input"), true);
        $idTitular = $_GET["idusuario"];
        $multi = $_GET["multi"];
        $vehiculoModel = new VehiculoRepository();

        if (!$multi) {

            $vehiculo = new Vehiculo(
                0,
                $request['matricula'],
                $request['marca'],
                $request['modelo'],
                $request['carroceria'],
                new DateTimeImmutable($request['anyo'])
            );
        } else {
            $vehiculo = $vehiculoModel->existeMatricula($request["matricula"]);

            if (count($vehiculo) > 0) {
                $vehiculo = $vehiculoModel->generateVehiculo($vehiculo);
            } else {
                $vehiculo = null;
            }
        }

        if ($vehiculo != null) {

            try {
                $vehiculoIngresado = $vehiculoModel->insertVehiculo($vehiculo);
                $titularidad = $vehiculoModel->insertTitularidad($vehiculoIngresado->getIdvehiculo(), $idTitular);
            } catch (PDOException $e) {
                $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
            }


            header('Content-Type: application/json');
            die(json_encode($vehiculoIngresado->getPublicData()));
        }
        header('Content-Type: application/json');
        die(json_encode(null));
    }

    public function deleteCarFromUser()
    {
        if (!isset($_GET['idcar'])) {
            $this->sendErrorMessage(400, 4004, "El id del vehiculo no existe");
        }
        if (!isset($_GET['idusuario'])) {
            $this->sendErrorMessage(400, 4004, "El id del usuario no existe");
        }

        $vehiculoId = $_GET['idcar'];
        $usuarioId = $_GET['idusuario'];

        $vehiculoModel = new VehiculoRepository();

        /* $vehiculoModel->deleteReservasVehiculo($vehiculoId, $usuarioId); */
        $vehiculoModel->deleteTitularidadVehiculo($vehiculoId, $usuarioId);
        $numPropietarios = $vehiculoModel->getNumPropietarios($vehiculoId);

        if ($numPropietarios == 0) {
            try {
                $affectedRows = $vehiculoModel->deleteVehiculo($vehiculoId);
            } catch (PDOException $e) {
                $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
            }

            if ($affectedRows <= 0) {
                $this->sendErrorMessage(200, 2001, "Vehículo no encontrado");
            }
        }

        $this->sendErrorMessage(201, 2002, "Vehículo eliminado correctamente");
    }

    public function actualizarVehiculo()
    {

        $vehiculoModel = new VehiculoRepository();
        $request = json_decode(file_get_contents("php://input"), true);

        try {

            $vehiculoBD = $vehiculoModel->getVehiculoRow($request["idvehiculo"]);

            if (empty($vehiculoBD)) {
                $this->sendErrorMessage(400, 2001, "Vehiculo no encontrado");
            }

            $vehiculoBD = $vehiculoModel->getVehiculo($vehiculoBD);

            if ($request["matricula"] != $vehiculoBD->getMatricula()) {
                $existMatricula = count($vehiculoModel->checkIfMatriculaExist($request["matricula"]));
            } else {
                $existMatricula = -1;
            }

            if ($existMatricula > 0) {
                $this->sendErrorMessage(400, 4001, "Matrícula ya existe");
            }

            $vehiculoBD->setMarca($request['marca']);
            $vehiculoBD->setModelo($request['modelo']);
            $vehiculoBD->setCarroceria($request['carroceria']);

            $vehiculoBD->setAnyo(new DateTimeImmutable($request['anyo']));

            $affectedRows = $vehiculoModel->updateVehiculo($vehiculoBD);
        } catch (Exception $e) {
            $this->sendErrorMessage((int)$e->getCode(), (int) $e->getCode(), $e->getMessage());
        }


        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($vehiculoBD->getPublicData()));
    }
}
