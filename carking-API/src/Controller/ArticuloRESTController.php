<?php
require_once(ENTITYPATH . "Faq.php");
require_once(REPOSITORYPATH . "ArticuloRepository.php");
require_once(CTRLPATH . 'CoreController.php');

class ArticuloRESTController extends CoreController
{

    public function __construct()
    {
    }

    public function getAllArticulos()
    {
        $articuloModel = new ArticuloRepository();
        $arrArticulos  = $articuloModel->getAllArticulos();
        $result    = null;
        foreach ($arrArticulos as $articulo) {
            $result[] = $articulo->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getLastArticulos()
    {
        $articuloModel = new ArticuloRepository();
        $arrArticulos  = $articuloModel->getLastArticulos();
        $result    = null;
        foreach ($arrArticulos as $articulo) {
            $result[] = $articulo->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getArticulo()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del artículo no existe");
        }

        $articuloId = $_GET['id'];

        $articuloModel = new ArticuloRepository();
        $articulo = $articuloModel->getArticuloRow($articuloId);
        $articulo = $articuloModel->getArticulo($articulo);
        header('Content-Type: application/json');
        die(json_encode($articulo->getPublicData()));
    }

    public function getArticulosId()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del artículo no existe");
        }

        $articuloId = $_GET['id'];

        $articuloModel = new ArticuloRepository();
        $articulos = $articuloModel->getArticulosId($articuloId);
        $result    = [];
        foreach ($articulos as $articulo) {
            $result[] = $articulo->getPublicData();
        }
        header('Content-Type: application/json');
        die(json_encode($result));
    }

    public function deleteArticulo()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del artículo no existe");
        }

        $artId = $_GET['id'];

        $articuloModel = new ArticuloRepository();


        try {
            $affectedRows = $articuloModel->deleteArticulo($artId);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($affectedRows <= 0) {
            $this->sendErrorMessage(200, 2001, "Artículo no encontrado");
        }

        $this->sendErrorMessage(201, 2002, "Artículo eliminado correctamente");
    }

    public function insertarArticulo()
    {
        $request = json_decode(file_get_contents("php://input"), true);

        $folderPath = UPLOADFOLDER;
        $image_parts = explode(";base64,", $request['imgsrc']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . uniqid() . '.png';


        file_put_contents($file, $image_base64);

        $articulo = new Articulo(
            0,
            $request['titulo'],
            $request['contenido'],
            ROOTFOLDER . $file,
            $request['idautor'],
            new DateTimeImmutable()
        );

        $articuloModel = new ArticuloRepository();

        try {
            $articuloIngresado = $articuloModel->insertArticulo($articulo);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }


        header('Content-Type: application/json');
        die(json_encode($articuloIngresado->getPublicData()));
    }

    public function modificarArticulo()
    {
        $articuloModel = new ArticuloRepository();
        $request = json_decode(file_get_contents("php://input"), true);
        try {
            $artBD = $articuloModel->getArticuloRow($request["idarticulo"]);
            if (empty($artBD)) {
                $this->sendErrorMessage(400, 2001, "Artículo no encontrado");
            }

            $folderPath = UPLOADFOLDER;
            $image_parts = explode(";base64,", $request['imgsrc']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . uniqid() . '.png';
    
    
            file_put_contents($file, $image_base64);
    


            $artBD = $articuloModel->getArticulo($artBD);
            $artBD->setTitulo($request["titulo"]);
            $artBD->setContenido($request["contenido"]);
            $artBD->setImgsrc(ROOTFOLDER . $file);
            $affectedRows = $articuloModel->updateArticulo($artBD);
        } catch (Exception $e) {
            $this->sendErrorMessage((int)$e->getCode(), (int) $e->getCode(), $e->getMessage());
        }

        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($artBD->getPublicData()));
    }
}
