<?php
require_once(ENTITYPATH . "Plaza.php");
require_once(REPOSITORYPATH . "PlazaRepository.php");
require_once(CTRLPATH . 'CoreController.php');

class PlazaRESTController extends CoreController
{

    public function __construct()
    {
    }

    public function getAllPlazas()
    {
        $plazaModel = new PlazaRepository();
        $arrPlazas = $plazaModel->getAllPlazas();
        $result    = [];
        foreach ($arrPlazas as $plaza) {
            $result[] = $plaza->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getAllReservas()
    {
        $plazaModel = new PlazaRepository();
        $arrReservas = $plazaModel->getAllReservas();
        $result    = [];
        foreach ($arrReservas as $reserva) {
            $result[] = $reserva->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getPlazasId()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id de la plaza no existe");
        }

        $plazaId = $_GET['id'];

        $plazaModel = new PlazaRepository();
        $plazas = $plazaModel->getPlazasId($plazaId);
        $result    = [];
        foreach ($plazas as $plaza) {
            $result[] = $plaza->getPublicData();
        }
        header('Content-Type: application/json');
        die(json_encode($result));
    }

    public function getPlazasIdParking()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id del parking no existe");
        }

        $parkingId = $_GET['id'];

        $plazaModel = new PlazaRepository();
        $plazas = $plazaModel->getPlazasIdParking($parkingId);
        $result    = [];
        foreach ($plazas as $plaza) {
            $result[] = $plaza->getPublicData();
        }
        header('Content-Type: application/json');
        die(json_encode($result));
    }

    public function getPlaza()
    {
        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id de la plaza no existe");
        }

        $plazaId = $_GET['id'];

        $plazaModel = new PlazaRepository();
        $plaza = $plazaModel->getPlazaRow($plazaId);
        $plaza = $plazaModel->getPlaza($plaza);
        header('Content-Type: application/json');
        die(json_encode($plaza->getPublicData()));
    }
    /*
        ESTADO PLAZAS
            0 - libre
            1 - ocupada
            2 - reservada
        */
    public function reservaPlaza()
    {
        $request = json_decode(file_get_contents("php://input"), true);

        $plazaModel = new PlazaRepository();
        $reserva = $plazaModel->crearReserva(intval($request["idusuario"]), intval($request["idplaza"]), $request["codigo"]);

        $plazaModel->modificarEstadoPlaza(intval($request["idplaza"]), 2);

        header('Content-Type: application/json');
        die(json_encode($reserva));
    }

    public function anularReserva()
    {
        $request = json_decode(file_get_contents("php://input"), true);
        $plazaModel = new PlazaRepository();
        $idplaza = intval($request["idplaza"]);
        $estado = intval($request["estado"]);
        $reserva = $plazaModel->anularReserva($request["codigoreserva"]);
        $numFilas = $plazaModel->modificarEstadoPlaza($idplaza, $estado);

        /* header('Content-Type: application/json');
        die(json_encode($reserva)); */
    }

    public function getReservasUsuario()
    {
        $idusuario = intval($_GET["idusuario"]);
        $plazaModel = new PlazaRepository();
        $arrReservas = $plazaModel->getReservasUsuario($idusuario);
        $result    = null;
        foreach ($arrReservas as $reserva) {
            $result[] = $reserva->getPublicData();
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function deletePlaza()
    {

        if (!isset($_GET['id'])) {
            $this->sendErrorMessage(400, 4004, "El id de la plaza no existe");
        }

        $plazaId = $_GET['id'];

        $plazaModel = new PlazaRepository();


        try {
            $affectedRows = $plazaModel->deletePlaza($plazaId);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }

        if ($affectedRows <= 0) {
            $this->sendErrorMessage(200, 2001, "Plaza no encontrada");
        }

        $this->sendErrorMessage(201, 2002, "Plaza eliminada correctamente");
    }

    public function insertarPlaza()
    {
        $request = json_decode(file_get_contents("php://input"), true);

        $plaza = new Plaza(
            0,
            $request['idparking'],
            $request['planta'],
            $request['codigo'],
            0,
            $request['tipo'],
            $request['longitud'],
            $request['latitud']
        );

        $plazaModel = new PlazaRepository();

        try {
            $plazaInsertada = $plazaModel->insertarPlaza($plaza);
        } catch (PDOException $e) {
            $this->sendErrorMessage(500, $e->getCode(), $e->getMessage());
        }


        header('Content-Type: application/json');
        die(json_encode($plazaInsertada->getPublicData()));
    }

    public function modificarPlaza()
    {
        $plazaModel = new PlazaRepository();
        $request = json_decode(file_get_contents("php://input"), true);

        try {
            $plazaBD = $plazaModel->getPlazaRow($request["idplaza"]);
            if (empty($plazaBD)) {
                $this->sendErrorMessage(400, 2001, "Plaza no encontrada");
            }
            $plazaBD = $plazaModel->getPlaza($plazaBD);

            $plazaBD->setIdParking($request["idparking"]);
            $plazaBD->setPlanta($request["planta"]);
            $plazaBD->setCodigo($request["codigo"]);
            $plazaBD->setEstado($request["estado"]);
            $plazaBD->setTipo($request["tipo"]);
            $plazaBD->setLongitud($request["longitud"]);
            $plazaBD->setLatitud($request["latitud"]);

            $affectedRows = $plazaModel->updatePlaza($plazaBD);
        } catch (Exception $e) {
            $this->sendErrorMessage((int)$e->getCode(), (int) $e->getCode(), $e->getMessage());
        }

        http_response_code(201);
        header('Content-Type: application/json');
        die(json_encode($plazaBD->getPublicData()));
    }
}
