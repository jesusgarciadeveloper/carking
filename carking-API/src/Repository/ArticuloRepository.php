<?php

require_once(ENTITYPATH . "Articulo.php");
class ArticuloRepository extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllArticulos(): array
    {
        $query    = 'SELECT * FROM articulo';
        $response = parent::getArrayRows($query);

        $result = null;
        foreach ($response as $art) {
            $result[] = $this->generateArticulo($art);
        }

        return $result;
    }

    public function getLastArticulos(): array
    {
        $query    = 'SELECT * FROM `articulo` ORDER BY fechapub DESC LIMIT 3';
        $response = parent::getArrayRows($query);

        $result = null;
        foreach ($response as $art) {
            $result[] = $this->generateArticulo($art);
        }

        return $result;
    }

    public function getArticuloRow(int $id): array
    {
        $sql = "SELECT * FROM articulo WHERE idarticulo = :id";
        $response = parent::getRow($sql, array("id" => $id));

        return $response;
    }

    public function getArticulosId(int $id): array
    {
        $sql = "SELECT * FROM articulo WHERE idarticulo LIKE '%$id%'";
        $response = parent::getArrayRows($sql);
        $result = [];
        foreach ($response as $art) {
            $result[] = $this->generateArticulo($art);
        }

        return $result;
    }

    public function getArticulo(array $articuloRow): Articulo
    {
        return $this->generateArticulo($articuloRow);
    }

    public function generateArticulo(array $articulo): Articulo
    {
        return new Articulo(
            $articulo["idarticulo"],
            $articulo["titulo"],
            $articulo["contenido"],
            $articulo["imgsrc"],
            $articulo["idautor"],
            new DateTimeImmutable($articulo["fechapub"])
        );
    }

    public function insertArticulo(Articulo $articulo): Articulo
    {
        $idautor   = $articulo->getIdAutor();
        $titulo   = $articulo->getTitulo();
        $contenido   = $articulo->getContenido();
        $imgsrc   = $articulo->getImgsrc();
        $fechapub = $articulo->getFechapub()->format("Y-m-d H:i:s");

        $sql = "INSERT INTO `articulo`( 
            `idautor`, `titulo`, `contenido`,`imgsrc`,`fechapub`) 
                VALUES (
                        '$idautor',
                        '$titulo', 
                        '$contenido',
                        '$imgsrc',
                        '$fechapub'
                        )";

        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return $this->generateArticulo(parent::getRow("SELECT * FROM articulo WHERE idarticulo = " . $this->connection->lastInsertId()));
        }
    }

    public function updateArticulo(Articulo $articulo): int
    {
        $sql = "UPDATE articulo 
        SET idautor = :idautor,
            titulo = :tit,
            contenido = :con,
            imgsrc = :img
        WHERE idarticulo = :uuid";

        $array = array(
            "uuid"  => $articulo->getIdarticulo(),
            "idautor"   => $articulo->getIdAutor(),
            "tit"   => $articulo->getTitulo(),
            "con"   => $articulo->getContenido(),
            "img"   => $articulo->getImgsrc()
        );


        return parent::execQuery($sql, $array);
    }

    public function deleteArticulo(string $articuloId): int
    {
        $sql = "DELETE FROM articulo WHERE idarticulo = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $articuloId]);
        return $response;
    }
}
