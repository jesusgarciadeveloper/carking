<?php

require_once(ENTITYPATH . "Faq.php");
class FaqRepository extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllFaqs(): array
    {
        $query    = 'SELECT * FROM faq';
        $response = parent::getArrayRows($query);

        $result = [];
        foreach ($response as $faq) {
            $result[] = $this->generateFaq($faq);
        }

        return $result;
    }

    public function getFaqsId(int $id): array
    {
        $sql = "SELECT * FROM faq WHERE idfaq LIKE '%$id%'";
        $response = parent::getArrayRows($sql);
        $result = [];
        foreach ($response as $faq) {
            $result[] = $this->generateFaq($faq);
        }

        return $result;
    }

    public function getFaqRow(int $id): array
    {
        $sql = "SELECT * FROM faq WHERE idfaq = :id";
        $response = parent::getRow($sql, array("id" => $id));

        return $response;
    }

    public function getFaq(array $faqRow): Faq
    {
        return $this->generateFaq($faqRow);
    }

    public function generateFaq(array $faq): Faq
    {
        return new Faq(
            $faq["idfaq"],
            $faq["idautor"],
            $faq["pregunta"],
            $faq["respuesta"]
        );
    }

    public function insertarFaq(Faq $faq): Faq
    {
        $idautor   = $faq->getIdAutor();
        $pregunta   = $faq->getPregunta();
        $respuesta   = $faq->getRespuesta();

        $sql = "INSERT INTO `faq`( 
            `idautor`, `pregunta`, `respuesta`) 
                VALUES (
                        '$idautor',
                        '$pregunta', 
                        '$respuesta')";

        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return $this->generateFaq(parent::getRow("SELECT * FROM faq WHERE idfaq = " . $this->connection->lastInsertId()));
        }
    }

    public function updateFaq(Faq $faq): int
    {
        $sql = "UPDATE faq 
        SET idautor = :idautor,
            pregunta = :pre,
            respuesta = :res
        WHERE idfaq = :uuid";

        $array = array(
            "uuid"  => $faq->getIdfaq(),
            "idautor"   => $faq->getIdAutor(),
            "pre"   => $faq->getPregunta(),
            "res"   => $faq->getRespuesta()
        );


        return parent::execQuery($sql, $array);
    }

    public function deleteFaq(string $faqId): int
    {
        $sql = "DELETE FROM faq WHERE idfaq = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $faqId]);
        return $response;
    }
}
