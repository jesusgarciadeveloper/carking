<?php

require_once(ENTITYPATH . "Plaza.php");
require_once(ENTITYPATH . "Reserva.php");

class PlazaRepository extends CoreModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPlazaRow(int $id): array
    {
        $sql = "SELECT * FROM plaza WHERE idplaza = :id";
        $response = parent::getRow($sql, array("id" => $id));

        return $response;
    }

    public function getPlaza(array $plazaRow): Plaza
    {
        return $this->generatePlaza($plazaRow);
    }

    private function generatePlaza(array $request): Plaza
    {
        return new Plaza(
            $request['idplaza'],
            $request['idparking'],
            $request['planta'],
            $request['codigo'],
            $request['estado'],
            $request['tipo'],
            $request['longitud'],
            $request['latitud']
        );
    }

    private function generateReserva(array $request): Reserva
    {
        return new Reserva(
            $request['codigoreserva'],
            $request['idusuario'],
            $request['idplaza'],
            new DateTimeImmutable($request['diahora'])
        );
    }

    public function getAllPlazas(): array
    {
        $query    = 'SELECT * FROM plaza ORDER BY idparking';
        $response = parent::getArrayRows($query);

        $result = [];
        foreach ($response as $plaza) {
            $result[] = $this->generatePlaza($plaza);
        }

        return $result;
    }

    public function getPlazasId(int $id): array
    {
        $sql = "SELECT * FROM plaza WHERE idplaza = $id";
        $response = parent::getArrayRows($sql);
        $result = [];
        foreach ($response as $pla) {
            $result[] = $this->generatePlaza($pla);
        }

        return $result;
    }

    public function getPlazasIdParking(int $id): array
    {
        $sql = "SELECT * FROM plaza WHERE idparking LIKE '%$id%' ORDER BY idparking";
        $response = parent::getArrayRows($sql);
        $result = [];
        foreach ($response as $pla) {
            $result[] = $this->generatePlaza($pla);
        }

        return $result;
    }

    public function deletePlaza(string $plazaId): int
    {
        $sql = "DELETE FROM plaza WHERE idplaza = :uuid";
        $response = parent::execQuery($sql, [":uuid" => $plazaId]);
        return $response;
    }

    public function insertarPlaza(Plaza $plaza): Plaza
    {
        $idparking   = $plaza->getIdParking();
        $planta   = $plaza->getPlanta();
        $codigo   = $plaza->getCodigo();
        $estado   = $plaza->getEstado();
        $tipo   = $plaza->getTipo();
        $longitud   = $plaza->getLongitud();
        $latitud   = $plaza->getLatitud();

        $sql = "INSERT INTO `plaza`
                            (`idparking`, `planta`, `codigo`,
                            `estado`, `tipo`, `longitud`, `latitud`) 
                    VALUES (
                        '$idparking', 
                        '$planta',
                        '$codigo',
                        '$estado',
                        '$tipo',
                        '$longitud',
                        '$latitud'
                        )";

        $numFilas = parent::execQuery($sql);

        if ($numFilas > 0) {
            return $this->generatePlaza(parent::getRow("SELECT * FROM plaza WHERE idplaza = " . $this->connection->lastInsertId()));
        }
    }

    public function updatePlaza(Plaza $plaza): int
    {
        $sql = "UPDATE `plaza` 
        SET `idparking`=:idparking,
            `planta`=:planta,
            `codigo`=:codigo,
            `estado`=:estado,
            `tipo`=:tipo,
            `longitud`=:longitud,
            `latitud`=:latitud 
        WHERE idplaza = :uuid";

        $array = array(
            "uuid"          => $plaza->getIdplaza(),
            "idparking"     => $plaza->getIdParking(),
            "planta"        => $plaza->getPlanta(),
            "codigo"        => $plaza->getCodigo(),
            "estado"        => $plaza->getEstado(),
            "tipo"          => $plaza->getTipo(),
            "longitud"       => $plaza->getLongitud(),
            "latitud"       => $plaza->getLatitud()
        );


        return parent::execQuery($sql, $array);
    }

    public function crearReserva(int $idusuario, int $idplaza, string $codigoReserva)
    {
        $sql = "INSERT INTO `reserva`(`idusuario`, `idplaza`, `codigoreserva`) VALUES (:idusu,:idpla,:codi)";
        $array = array(
            "idusu"     => $idusuario,
            "idpla"     => $idplaza,
            "codi"      => $codigoReserva
        );
        $numFilas = parent::execQuery($sql, $array);


        return parent::getRow("SELECT * FROM reserva WHERE idusuario = " . $idusuario);
    }

    public function getAllReservas(): array
    {
        $query    = 'SELECT * FROM reserva ORDER BY idusuario';
        $response = parent::getArrayRows($query);

        $result = [];
        foreach ($response as $reserva) {
            $result[] = $this->generateReserva($reserva);
        }

        return $result;
    }

    public function modificarEstadoPlaza(int $idplaza, int $estado): int
    { /*
        ESTADO PLAZAS
            0 - libre
            1 - ocupada
            2 - reservada
        */
        $sql = "UPDATE `plaza` 
        SET `estado`=:estado 
        WHERE idplaza = :uuid";

        $array = array(
            "uuid"      => $idplaza,
            "estado"    => $estado
        );

        return parent::execQuery($sql, $array);
    }

    public function anularReserva(string $codigoReserva)
    {
        //$reserva = parent::getRow("SELECT * FROM reserva WHERE codigoreserva = $codigoReserva");

        $sql = "DELETE 
                FROM `reserva`  
                WHERE codigoreserva = :cod";

        $array = array(
            "cod" => $codigoReserva
        );

        parent::execQuery($sql, $array);
        //return $reserva;
    }

    public function getReservasUsuario(int $idusuario)
    {
        $sql = "SELECT * FROM reserva WHERE idusuario = :idusu";
        $array = array("idusu" => $idusuario);
        $response = parent::getArrayRows($sql, $array);

        $result = null;
        foreach ($response as $reserva) {
            $result[] = $this->generateReserva($reserva);
        }

        return $result;
    }
}
